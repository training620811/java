public class Example1 {
    public static void main(String[] args) {

        //1- {1.2, 1.3, 5.6, 4.3,10.2,0.5,5.8,9.1} listesindeki,
        //   sayilari yazdirin,
        //   toplamlarini yazdirin,
        //   en buyuk ile en kucuk sayiyilari yazdirin.

        double[] myList = {1.2, 1.3, 5.6, 4.3, 10.2, 0.5, 5.8, 9.1};

        double total = 0;
        double biggestNum = myList[0];
        double smallestNum = myList[0];


        for (int i = 0; i < myList.length; i++) {
            if (myList[i] > biggestNum) {
                biggestNum = myList[i];
            }
            if (myList[i] < smallestNum) {
                smallestNum = myList[i];
            }
            System.out.println(myList[i]);   // arrayleri array class ı kullanmadan loopla
            total += myList[i];              // listesiz alt alta yazdirabiliriz.
        }
        System.out.println("Toplam = " + total);
        System.out.println("En buyuk sayi = " + biggestNum);
        System.out.println("En kucuk sayi = " + smallestNum);


    }
}