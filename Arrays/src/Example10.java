import java.util.Arrays;

public class Example10 {
    public static void main(String[] args) {

        //10- Verilen bir array'deki tum elementleri bir sola kaydırıp, bastaki elementi de
        //   sona tasiyacak bir method olusturun, array'i yeni haliyle kaydedin
        //   orn: input :[4,5,6,7] array'ın son hali :[5,6,7,4]

        int [] arr ={4,5,6,7};
        arr =solaKaydir(arr);
        System.out.println(Arrays.toString(arr));

    }
    public static int[] solaKaydir(int [] array){
        int emptyBox = array[0];

        for (int i = 0; i <array.length-1 ; i++) {
            array[i]=array[i+1];
        }
        array[array.length-1]=emptyBox;
        return array;
    }
}
