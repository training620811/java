import java.util.Arrays;

public class Example11 {
    public static void main(String[] args) {

        //11-Verilen bir int array'de en kucuk ve en buyuk sayilari
        // yazdiran bir method olusturun.

        int[] arr={3,8,1,5,2,9};
        enBuyukEnkucukSayiYazdir(arr);
    }
    public static void enBuyukEnkucukSayiYazdir (int[] array){
        Arrays.sort(array); //siraladik.
        System.out.println("En buyuk sayi = " + array[array.length-1]);
        System.out.println("En kucuk sayi = " + array[0]);
    }
}
