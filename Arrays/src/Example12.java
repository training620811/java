import java.util.Arrays;

public class Example12 {
    public static void main(String[] args) {

        //12- Kullanıcıdan alınan bir string ifadede kac tane e harfi oldugunu bulan bir method yazınız.

        String str = "Java gercekten cok cok guzel";

        String [] eArray= str.split("e");

        System.out.println(Arrays.toString(eArray));
        System.out.println(eArray.length);
        System.out.println("Metindeki e sayisi : " + (eArray.length-1));

        // Stringler Arraylere cevrilir. Bu ornekte e den parcaladık. 4 ayrac 5 parca oldu.
        // e sayısı da eleman sayısının bır eksıgı oldu.

        System.out.println("");

        //2.yontem
        //hiclik ile ("") split yaparak ayrırız.

        String [] hicArray =str.split("");
        System.out.println(Arrays.toString(hicArray));

        int count=0;
        for (int i = 0; i <hicArray.length ; i++) {
            if (hicArray[i].equals("e")){
                count++;
            }
        }
        System.out.println("Metindeki e sayisi : " + count);

        System.out.println();

        System.out.println("Metindeki e sayisi : " + eSayisiBulma(str));


    }

    public static int eSayisiBulma(String str){
        String arr [] = str.split("");

        int count =0 ;
        for (int i = 0; i <arr.length ; i++) {
            if (arr[i].equalsIgnoreCase("e")){
                count++;
            }
        }
        return count;
    }
}
