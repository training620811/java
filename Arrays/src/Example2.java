import java.util.Arrays;
import java.util.Scanner;
public class Example2 {
    public static void main(String[] args) {

        //2- Kullanicidan deger alarak array olustur ve yazdir.
        //   Alınan sayilarin ortalamasini yazdir.

        Scanner scan = new Scanner(System.in);

        int[] userArray = new int[5];
        double ort;
        double total = 0;

        for (int i = 0; i < userArray.length; i++) {
            System.out.println("Listeye eklenecek " + (i + 1) + ". sayiyi giriniz :");
            userArray[i] = scan.nextInt();
        }
        System.out.println("Kullanici listesi : " + Arrays.toString((userArray)));
        // Array leri loopsuz array class ını kullanarak liste seklinde yazdirabiliriz.

        for (int num : userArray) {
            total += num;
        }
        System.out.println("Toplam = " + total);
        ort = total / userArray.length;
        System.out.println("Ortalama = " + ort);
    }
}
