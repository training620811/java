import java.util.Arrays;
public class Example3 {
    public static void main(String[] args) {

        //3- Verilen bir int Array'in tum elemanlarini 2 artırıp bize donduren bir metod olusturun. Eski array'i
        //   yeni haliyle kaydedin.

        int[] arr = {2, 4, 6, 8};

        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] + 2;
        }
        System.out.println(Arrays.toString(arr)); // [4, 6, 8, 10]

        arr=elementleri2Artir(arr);
        System.out.println(Arrays.toString(arr)); // [6, 8, 10, 12] eski arr yi metod kullanarak 2 artırdı guncelledi.
    }

    public static int[] elementleri2Artir(int[] arr) {  // metod (fonksiyon) olusturduk.
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] + 2;
        }
        return arr;
    }
}
