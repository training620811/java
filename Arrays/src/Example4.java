import java.util.Arrays;
public class Example4 {
    public static void main(String[] args) {

        //4- Verilen bir array’deki pozitif tamsayilari toplayip sonucu bize donduren bir method yaziniz.

        int [] arr = {3,-3,5,-7,-6,1,-2};

        int total=0;

        for (int i = 0; i <arr.length ; i++) {
            if (arr[i]>0){
                total+=arr[i];
            }
        }
        System.out.println("Liste : " + Arrays.toString(arr));
        System.out.println("Listedeki pozitif sayilar toplamı : " + total); //main metotla cozduk.

        int result; // metotla cozduk.
        result =pozitifElementlerToplami(arr);
        System.out.println("Listedeki pozitif sayilar toplamı : "+ result);

    }

    public static int pozitifElementlerToplami(int [] arr){ // metod(fonksiyon) olusturduk.
        int total=0;

        for (int i = 0; i <arr.length ; i++) {
            if (arr[i]>0){
                total+=arr[i];
            }
        } return total;
    }
}
