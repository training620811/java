import java.util.Arrays;
import java.util.Scanner;

public class Example5 {
    public static void main(String[] args) {

        //5- Verilen bir array’de kullanicidan istenen bir elemanin var olup olmadigini
        //   ve varsa kac kere kullanildigini yazdiran bir method olusturun.

        int[] arr = {1, 2, 4, 5, 3, 6, 7, 4, 2, 3, 5, 1, 3, 2};

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen bir sayi giriniz :");
        int userNum = scan.nextInt();

        int i = 0, count = 0;
        while (i < arr.length) {
            if (arr[i] == userNum) {
                count += 1;
            }
            i++;
        }
        System.out.println("Liste : " + Arrays.toString(arr));  //main metotla cozduk.
        System.out.println("İstenen sayi : " + userNum);
        System.out.println(userNum + " istenen sayisi " + count + " kere kullanilmistir.");

        System.out.println();

        // metod(fonksiyon) ile cozduk.

        System.out.println("Liste : " + Arrays.toString(arr));
        System.out.println("İstenen sayi : " + userNum);
        userNumInListRepeat(arr, userNum);

    }

    public static void userNumInListRepeat(int[] array, int scan) { //metot olusturduk ama void

        int i = 0, count = 0;
        while (i < array.length) {
            if (array[i] == scan) {
                count += 1;
            }
            i++;
        }
        if (count == 0) {
            System.out.println(scan + " sayisi array'de yoktur.");
        } else {
            System.out.println(scan + " sayisi array'de vardır.");
        }
        System.out.println(scan + " istenen sayisi " + count + " kere kullanilmistir.");
    }
}
