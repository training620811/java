import java.util.Arrays;
import java.util.Scanner;

public class Example6 {
    public static void main(String[] args) {

        //6- Kullanicidan array’in boyutunu ve elementlerini alip array’i olusturan
        // ve bize donduren bir method olusturun.


        int[] result = arrayCreate();
        System.out.println("Kullanicinin olusturdugu Array : " + Arrays.toString(result));
    }

    public static int[] arrayCreate() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Array'in elaman sayisini (boyutunu) giriniz :");
        int userArrayLength = scan.nextInt();

        int[] userArray = new int[userArrayLength];

        for (int i = 0; i < userArray.length; i++) {
            System.out.println("Lutfen " + (i + 1) + ". sayiyi giriniz :");
            userArray[i] = scan.nextInt();
        }
        return userArray;
    }
}
