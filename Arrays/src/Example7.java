public class Example7 {
    public static void main(String[] args) {

        //7- Verilen String bir array’deki en uzun ve en kisa kelimeleri yazdiran
        // bir method olusturun.

        String[] arr = {"Hasan", "Adem", "Senturk", "Omer Faruk"};

        String enuzunKelime = arr[0];
        String enkisaKelime = arr[0];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length() > enuzunKelime.length()) {
                enuzunKelime = arr[i];
            }
            if (arr[i].length() < enkisaKelime.length()) {
                enkisaKelime = arr[i];
            }
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
        System.out.println("En uzun kelime : " + enuzunKelime);
        System.out.println("En kisa kelime : " + enkisaKelime);


    }
}

