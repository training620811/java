import java.util.Arrays;

public class Example9 {
    public static void main(String[] args) {

        //9- Verilen bir array'deki tum elementleri bir saga kaydırıp, sonraki elementi de
        //   basa tasiyin, array'i yeni haliyle kaydedin
        //   orn: input :[4,5,6,7] array'ın son hali :[7,4,5,6]

        int [] arr = {4,5,6,7};

        System.out.println("Liste = "+Arrays.toString(arr));

        int emptyArr= arr[arr.length-1];
        for (int i =arr.length-2; i>=0;i--){
            arr[i+1]=arr[i];
        }
        arr[0]=emptyArr;

        System.out.println("Guncel liste = "+Arrays.toString(arr));

    }
}
