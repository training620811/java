import java.util.Arrays;
public class Explanation {
    public static void main(String[] args) {

        //1- {2,4,6,8,10} array in bir elementine ulasmak ve uptade etmek.

        int[] arr = {2, 4, 6, 8, 10};

        System.out.println(arr[0]); //2
        System.out.println(arr[1]); //4
        System.out.println(arr[2]);
        System.out.println(arr[3]);
        System.out.println(arr[4]); //10

        arr[3] = 20; // 4. elemani degistirdik.

        System.out.println(arr[3]); // 20

        System.out.println(arr.length); // array in eleman sayisi yani uzunlugu (5)

        System.out.println(arr[arr.length - 1]); // son elementi yazdirir. (10)

        for (int i = 0; i < arr.length; i++) {      // array in tum elementlerini yazdirir.
            System.out.print(arr[i] + " ");
        }

        String[] arr2 = {"Ali", "Veli", "Kazım"};

        // arr2 bir array olarak yazdirmak
        // array bir obje / non-primitive data oldugundan Java referansini yazdirir.
        // Array ı array olarak yazmak ıstersek Array class ından yardım alınmalidir.

        System.out.println(Arrays.toString(arr2)); // [Ali, Veli, Kazım]
    }
}