
import java.util.Date;

public abstract class Account {
    private int balance;
    private final int ID;

    public Account(int ID, int balance) {
        this.balance = balance;
        this.ID = ID;
    }

    public void deposit(int cash) {
        this.balance += cash;
    }

    public void withdraw(int cash) {
        if (cash > this.balance) {
            System.out.println("Hesabınızda " + cash + " kadar para bulunmamaktadır.");
        } else {
            this.balance -= cash;
        }
    }

    public double getBalance() {
        return this.balance;
    }

    public int getID() {
        return this.ID;
    }

    public abstract void benefit(int annualProfit);
}
