
import java.util.ArrayList;
import java.util.Scanner;

public class Bank {
    private Scanner scan;
    private ArrayList<Account> accounts;
    private String date;

    public Bank() {
        this.accounts = new ArrayList<>();
        this.date = "01.01.2020";
        this.scan = new Scanner(System.in);
    }

    public void getAccount() {
        for (Account account : accounts) {
            System.out.println(account);
        }
    }

    public String getDate() {
        return date;
    }

    public void addAccount(Account account) {
        this.accounts.add(account);
    }

    public void deposit(int ID, int cash) {
        for (Account account : this.accounts) {
            if (account.getID() == ID) {
                account.deposit(cash);
                System.out.println("Isleminiz basarıyla gerceklestirildi !");
            } else {
                System.out.println("Gecersiz ID ! İslem gerceklestirilemedi.");
            }
        }
    }

    public void withdraw(int ID, int cash) {
        for (Account account : this.accounts) {
            if (account.getID() == ID) {
                account.withdraw(cash);
                System.out.println("transaction isleminiz basarıyla gerceklesti");
            } else {
                System.out.println("Gecersiz ID ! İslem gerceklestirilemedi.");
            }
        }
    }

    public void Create_S_ID_balance() {
        String[] parts = takeInformationForCreateNewAccount().split(",");
        int ID = Integer.valueOf(parts[0]);
        int balance = Integer.valueOf(parts[1]);
        Account shortTerm = new ShortTermAccount(ID, balance);
        addAccount(shortTerm);
    }

    public void Create_L_ID_balance() {
        String[] parts = takeInformationForCreateNewAccount().split(",");
        int ID = Integer.valueOf(parts[0]);
        int balance = Integer.valueOf(parts[1]);
        Account shortTerm = new LongTermAccount(ID, balance);
        addAccount(shortTerm);
    }

    public void Create_O_ID_balance() {
        String[] parts = takeInformationForCreateNewAccount().split(",");
        int ID = Integer.valueOf(parts[0]);
        int balance = Integer.valueOf(parts[1]);
        Account shortTerm = new SpecialAccount(ID, balance);
        addAccount(shortTerm);
    }

    public void Create_C_ID_balance() {
        String[] parts = takeInformationForCreateNewAccount().split(",");
        int ID = Integer.valueOf(parts[0]);
        int balance = Integer.valueOf(parts[1]);
        Account shortTerm = new CurrentAccount(ID, balance);
        addAccount(shortTerm);
    }

    public String takeInformationForCreateNewAccount() {
        System.out.println("Lutfen hesap bilgilerinizi 'ID,Balance' formatında giriniz.");
        String information = scan.nextLine();
        return information;
    }

    public void increase_ID_cash() {
        String[] parts = takeInformationOfIDAndCash().split(",");
        int ID = Integer.valueOf(parts[0]);
        int cash = Integer.valueOf(parts[1]);
        deposit(ID, cash);
    }

    public void decrease_ID_cash() {
        String[] parts = takeInformationOfIDAndCash().split(",");
        int ID = Integer.valueOf(parts[0]);
        int cash = Integer.valueOf(parts[1]);
        withdraw(ID, cash);
    }

    public String takeInformationOfIDAndCash() {
        System.out.println("Lutfen ID ve islem yapılacak mitarı 'ID,Cash' formatında giriniz.");
        String information = scan.nextLine();
        return information;
    }


    public void start() {
        System.out.println("Yapmak istediginiz islemi giriniz.");
        int input = scan.nextInt();

        switch (input) {
            case 0:
                break;
            case 1:
                Create_S_ID_balance();
                break;
            case 2:
                Create_L_ID_balance();
                break;
            case 3:
                Create_O_ID_balance();
                break;
            case 4:
                Create_C_ID_balance();
                break;
            case 5:
                increase_ID_cash();
                break;
            case 6:
                decrease_ID_cash();
            case 7:
            case 8:
            case 9:
            case 10:
            default:
        }
    }
}
