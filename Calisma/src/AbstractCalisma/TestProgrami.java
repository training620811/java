package AbstractCalisma;

import java.util.ArrayList;

public class TestProgrami {
    public static void main(String[] args) {

        ArrayList<IUreme> canlilar=new ArrayList<>();
        // Canlilar canlilar = new Canlilar() ; // Canlilar class ı abstract class oldugu ıcın obje olusturulamaz.
                                                // Ama Data turu olarak kullanılabılır.

        Canlilar insan5  = new Insan ();

        Insan insan = new Insan();
        insan.beslenme();

        Insan insan2 = new Insan();
        insan2.beslenme("elma");


        Virus virus =new Virus();
        virus.beslenme();

        Canlilar insan1 = new Insan();
        insan1.beslenme();

        IUreme insan10 =new Insan();

        Bakteri bakteri =  new Bakteri();

        Hayvan hayvan = new Hayvan();
        Kadin kadin =new Kadin();
        Erkek erkek=new Erkek();

        Insan kadin2 =new Kadin();

        Insan erkek2 =new Erkek();

        canlilar.add(insan);
        canlilar.add(insan1);
        canlilar.add(insan2);
        canlilar.add(virus);
        canlilar.add(bakteri);
        canlilar.add(kadin);
        canlilar.add(erkek);
        canlilar.add(hayvan);

        for(IUreme ureme: canlilar){
            ureme.ureme();
        }
    }
}
