public class Example1 {
    public static void main(String[] args) {

        //1- {1.2, 1.3, 5.6, 4.3,10.2,0.5,5.8,9.1} listesindeki,
        //   sayilari yazdirin,
        //   toplamlarini yazdirin,
        //   en buyuk ile en kucuk sayiyilari yazdirin.

        double[] myLists = {1.2, 1.3, 5.6, 4.3, 10.2, 0.5, 5.8, 9.1};

        double toplam = 0;
        double maxNum = myLists[0];
        double minNum = myLists[0];


        for (int i = 0; i < myLists.length; i++) {

            toplam += myLists[i]; // toplam = toplam + myList[i]

            System.out.print(myLists[i] + " ");

            if (myLists[i] > maxNum) {
                maxNum = myLists[i];
            }
            if (myLists[i] < minNum) {
                minNum = myLists[i];
            }
        }
        System.out.println("");
        System.out.println("En buyuk sayi = " + maxNum);
        System.out.println("En kucuk sayi = " + minNum);
        System.out.println("Toplam = " + toplam);
    }
}