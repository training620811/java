public class Main {
    public static void main(String[] args) {

        Car car1 = new Car();

        /*
           Araba class'ından obje olusturmak icin ilk once o objeleri bellekte tutması icin referans
           olusturmak gerekir.
           Car : Referans tutan car1 verianle'ın type'dir. Ayrıca Class ismidir. (String, Array vs gibi)
           car1 : Car class'ından olusturulacak objenin referansini tutacak olan Veriable'dır.
           new : Obje olusturma keyword'udur.
           Car() : Car class'ının constructor'udur. Car class'ından obje olusturmak icin gereklidir.
        */

        car1.color = "Siyah";
        car1.doorAmount =4;
        car1.engine = 1600;
        car1.model = "Renoult Megane";

        System.out.println("1. Arabanın rengi : "+ car1.color);
        System.out.println("1. Arabanın kapı sayisi : "+ car1.doorAmount);
        System.out.println("1. Arabanın motor hacmi : "+ car1.engine);
        System.out.println("1. Arabanın modeli : "+ car1.model);



        Car car2 = new Car();
        car2.color = "Beyaz";
        car2.doorAmount =2;
        car2.engine = 1400;
        car2.model = "Renoult Clue";

    }
}