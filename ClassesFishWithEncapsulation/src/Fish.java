public class Fish {

    // member veriables - members
    // field, properties
    private String type;        // Access modifiers prıvate oldugu icin bulundugu class'da gorulur ve gecerlidir.
    private String name;        // Main de ya da baska yerlerde kullanabılmek ıcın encapsulatıon yapmak gerekir.
    private String color;       // Yani method olusturmak gerekir.
    private double length;      // Okumak icin getter methodu, yazmak icin ise setter methodu kullanılır.

    public String getType(){        // Baska class'larda private degiskenleri kullanabilmek (okuyabilmek) icin
        return type;                // "get Method" unu kullanırız.
                                    // Get method'una erisebilmek icin methodun Access Modifier'ini public yapmak zorundayız.

                                    // Method body'sınde "this" keyword'u (istege baglı) ıle class'da bulunan veriable'ı return ile
                                    // cagrılan baska class'lara dondurmus oluruz.
                                    // sout ile dondurdugumuz degeri yazdırarak consol'da okuyabılırız.
    }

    public void setType(String type){   // Baska class'larda private degiskenleri kullanabilmek (yazmak) icin
        this.type=type;                 // "set Method" unu kullanırız.
                                        // Set method'una erisebilmek icin methodun Access Modifier'ini public yapmak zorundayız.

                                        // Method body'sınde "this" keyword'u ıle class'da bulunan veriable
                                        // ile method'da bulunan aynı isimli veriable'ın ayırı verıable oldugunu belırtmıs oluruz.

                                        // Sonra "=" operatoru ıle artık class'daki veriable'ın referansını
                                        // method parametresi olan verianle ile tutmus oluruz. Yani assign etmis oluruz.

                                        // Access modifier'ı private olan verıable'a baska class'larda (main gibi) deger ataması
                                        // yapmak ıcın return type'ı void olan set methodunu kullanırız.
                                        // Boylece cagırdıgımız class'da set methodu aracılıgı ile private bir veriable'a ulasır
                                        // o degiskene assignment yapabılırız.
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void alive(){
        System.out.println("Balık canlı..");
    }

}
