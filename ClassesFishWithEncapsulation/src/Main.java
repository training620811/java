public class Main {
    public static void main(String[] args) {

        Fish fish1 = new Fish();

        fish1.setType("Tatlı su balıgı");       // assign ettik.
        fish1.setColor("Turuncu");
        fish1.setLength(5);
        fish1.setName("Japon");

        System.out.println("Balıgın turu : " + fish1.getType());
        System.out.println("Balıgın rengi : " + fish1.getColor());
        System.out.println("Balıgın uzunlugu (cm) : " + fish1.getLength());
        System.out.println("Balıgın adı : " + fish1.getName());

        fish1.alive();

    }
}