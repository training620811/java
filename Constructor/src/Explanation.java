public class Explanation {

    // constructor bir class tan obje olusturuldugunda ilk deger ataması yapmak (initiliaze) icin kullanılır.
    /* OOP konsept birden çok obje olusturma ihtiyacı oldugunda olusturulmak istenen objelere
       kalıp olması ve daha sonrasında bu kalıptan istediğimiz kadar obje uretmemizi saglar.*/
    // lego kalıbı ornegi.

    /* Bir okulda bir ogretmen icin; ogretmen classı olusturulur, yine bu classta obje olusturlur ve orada belirlenen
    ozellikler ( bu ozellikler icin veriable ve metodlar olusturulur) diger tum ogretmenlere uygulanır.
    Sonrasında aynı proje icindeki tum classlardan ogretmen objesi olusturulabilir.*/

    // Constructor nasıl kullanılır:
    // new keywordu ile birlikte kullanılır.   Scanner scan = new sacnner(); daki gibi.
    // otomatik olarak gelen objeler gibi, bunları da biz olusturmus oluyoruz.

    // Ogretmen ogr = new ogretmen();  objesinde esitligin sol tarafi obje, sağ tarafi deger
    //    obje            deger
    // objeye deger atamazsak kullanamayız. null pointer a esitlersek yine kullanamyız. Deger atamamız gerekir.

    //*Constructor calistiginda kaynak classa gidecek orada atanmis olan degerleri alip olusturulan objeye atayacak.*

    // Eger yeni objemizin farklı degerlere sahip olmasını istersek yeni degerleri objeye atamalıyız

    // Constructor declaration acisindan metoda cok benzer ama; constructor objeler ilk deger atmamıza izin veren kod
    // bloklaridir.

    // 2 sey cok onemli:
    // 1.Constructor ismi class ismi ile aynı olmak zorundadır.
    // 2.return type yoktur.  (İsmi aynı olsa bile return type varsa o metoddur.)

    // Access modifire constructor ile ilgili degil; diger classlardan constructore ulasmamak icindir.
    // constructor () icine parametre yazılması kullanıcıya birakilmistir.

    /*
       1. Default Constructor: Classin icinde constructor olusturulmadigi halde arka planda default construcyor
          calısır ve obje uretir. sadece ilk atanan degeri verir. Parametri yoktur, bodysinde kod yoktur.

       2. Parametresiz Constructor: Default constructor ile ayni ozelliklere sahiptir. farki bunu biz olustururuz ve
          istersek atama yapabiliriz. Ama kullanım acisindan degisen birsey olmaz.

       3. Parametreli Constructor: Constructor icinde bizim parametre olarak gonderdigimiz degerleri instance veriable
          lara atama yaparak, bizi her bir deger icin teke tek atama yapmaktan kurtarir.
     */

    /* isim = ism;
       soyisim = syism;
       brans = brns;

       esitligin sol tarafi instance veriable;  sag tarafi  parametre olarak gonderdigimiz degerlerdir.
       Boylece obje olustururken constructora verdigimiz ism, sysm, brns parametre olarak gidip body icerisinde
       instance veriable lara atanarak objenin ozellikleri olurlar.
    */

    // this kullanimi: java nin ozel anlam yukledigi bir keyworddur.
    // Buyuk projelerde parametre isimlerinin veriable isimleri ile ayni olması kodu anlmammız acindan bize
    // kolaylik saglar. Anca Java scope geregi bu veriabllar varken instance veriable lara gitmez.
    // Bu yüzden veriablein instance veriable oldugunu belirtmek icin basina this konur. Veya parametrelere farkli
    // isimler VERMELİYİZ.
}
