import java.util.Scanner;

public class DataCastingChar {
    public static void main(String[] args) {

        int a= 'a'+'b'; // a'nın ascii degeri 97 b'nın ascii degeri 98 dir.
        System.out.println(a); // int bır matematıksel verıable oldugu ıcın 97+98 = 195 olur.
        System.out.println('a'+'b');// '+' işareti toplama oldugu ıcın sonuc yıne 97+98=195 olur.

        System.out.println(""+'a'+'b');// ""(hiclik) eklersek o zaman concatenate yapar ve ab olur


        // Ör: Kullanıcıdan bir char alın
        // ascii tablosunda bu karakterden sonraki üç karakteri yazdırın.

        Scanner scan= new Scanner(System.in);
        System.out.println("Lutfen bir karakter giriniz :");
        char userChar= scan.next().charAt(0);

        System.out.println("Girilen karakter : " + userChar );
        System.out.println("Girilen karakterin ascii karsiligi :" + (int)userChar);
        System.out.println("Girilen karakterden bir sonraki karakter :" + (char)(userChar+1));
        System.out.println("Girilen karakterden iki sonraki karakter :" + (char)(userChar+2));
        System.out.println("Girilen karakterden uc sonraki karakter :" + (char)(userChar+3));


    }
}
