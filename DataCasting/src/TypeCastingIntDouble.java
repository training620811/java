import java.util.Scanner;

public class TypeCastingIntDouble {
    public static void main(String[] args) {
        //Kullanıcıdan 2 tam sayı alıp, bunları bölün ve sonucu double olarak yazdırın.

        Scanner scan= new Scanner(System.in);

        System.out.println("Lütfen ilk sayıyı giriniz :");
        int userNum1= scan.nextInt();

        System.out.println("Lütfen ikinci sayıyı giriniz :");
        int userNum2= scan.nextInt();

        System.out.println("a/b nin değeri : " + userNum1/userNum2 );

        double  result=(double)userNum1/userNum2;
        System.out.println("Girilen sayıların bolumunun double karsılıgı : " + result);


    }
}