import java.util.Scanner;

public class Example3_SifreKontrolu {
    public static void main(String[] args) {

        /*
          3-Kullanicidan bir sifre girmesini isteyin.
            Girilen sifreyi asagidaki sartlara gore kontrol edin ve sifredeki hatalari yazdirin.
            Kullanici gecerli bir sifre girinceye kadar bu islemi tekrar edin
            ve gecerli sifre girdiginde “Sifreniz Kabul edilmistir” yazdirin.
                - Sifre kucuk harf icermelidir
                - Sifre buyuk harf icermelidir
                - Sifre ozel karakter icermelidir
                - Sifre en az 8 karakter olmalidir.
         */
        Scanner scan = new Scanner(System.in);
        String userPassword;
        int flagKucukHarf, flagBuyukHarf, flagSekizKarakter, flagOzelKarakter, flagCikisKontrol;
        boolean isPaswordTrue = false;

        do {
            System.out.println("Lutfen bir sifre giriniz");
            userPassword = scan.nextLine();


            flagKucukHarf = 0;
            for (int i = 0; i < userPassword.length(); i++) {
                if (userPassword.charAt(i) >= 'a' && userPassword.charAt(i) <= 'z') {
                    flagKucukHarf++;
                    break;
                }
            }
            if (flagKucukHarf == 0) {
                System.out.println("Sifre kucuk harf icermelidir.");
            }


            flagBuyukHarf = 0;
            for (int i = 0; i < userPassword.length(); i++) {
                if (userPassword.charAt(i) >= 'A' && userPassword.charAt(i) <= 'Z') {
                    flagBuyukHarf++;
                    break;
                }
            }
            if (flagBuyukHarf == 0) {
                System.out.println("Sifre buyuk harf icermelidir.");
            }


            flagSekizKarakter = 0;
            if ((userPassword.length() >= 8)) {
                flagSekizKarakter++;
            }
            if (flagSekizKarakter == 0) {
                System.out.println("Sifre en az 8 karakterli olmalidir.");
            }


            flagOzelKarakter = 0;
            if (!(userPassword.replaceAll("\\w", "").equals(""))) {
                flagOzelKarakter++;
            }

            if (flagOzelKarakter == 0) {
                System.out.println("Sifre ozel karakter icermelidir.");
            }


            flagCikisKontrol = flagKucukHarf + flagBuyukHarf + flagSekizKarakter + flagOzelKarakter;
            if (flagCikisKontrol == 4) {
                System.out.println("Sifreniz kabul edilmistir.");
                isPaswordTrue = true;
            }

        } while (!isPaswordTrue);
    }
}
