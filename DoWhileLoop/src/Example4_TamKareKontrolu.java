import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Example4_TamKareKontrolu {
    public static void main(String[] args) {

        //4- Kullanicidan bir pozitif sayi isteyin, sayinin tam kare olup olmadıgını bulunuz,
        //   tam kare ise true degılse false yazdırınız.
        //   ornek :        input : 16, output : 4

        Scanner scan=new Scanner(System.in);
        System.out.print("Lutfen bir sayi giriniz : ");
        int userNum = scan.nextInt();
        tamKareKontrolu(userNum);
    }

    private static void tamKareKontrolu(int num) {
        int baslangicSayisi = 1;
        boolean tamKareMi = false;
        do {

            if (baslangicSayisi * baslangicSayisi == num) {
                tamKareMi =true;
                break;
            }
            baslangicSayisi++;
        }while (baslangicSayisi*baslangicSayisi<=num);
        System.out.println(tamKareMi);
    }
}
