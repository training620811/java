public class Main {
    public static void main(String[] args) {

        // 1- 10-11-12 sayilarını toplayan bir do-while loop olusturun

        int sayi =10;
        int toplam =0;

        do {
            toplam+=sayi;
            sayi++;
        }while (sayi<=12);
        System.out.println(toplam);
    }
}