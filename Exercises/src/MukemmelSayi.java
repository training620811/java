import java.util.Scanner;

public class MukemmelSayi {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen sayinizi giriniz :");
        int userNum = scan.nextInt();

        int total = 0;

        for (int i = 1; i < userNum; i++) {
            if (userNum % i == 0) {
                total += i;
            }
        }
        if (total == userNum) {
            System.out.println(userNum + " mukemmel sayidir.");
        } else {
            System.out.println(userNum + " mukemmel sayi degildir.");
        }
    }
}
