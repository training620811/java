import java.util.Scanner;

public class Example10 {
    public static void main(String[] args) {

        //9-Kullanicidan pozitif bir sayi alin, 1’den baslayarak tum tamsayilari yazdirin,sıra
        //  - 3 ile bolunebilen bir sayiya gelirse, sayi yerine fizz
        //  -5 ile bolunebilen bir sayiya gelirse sayi yerine buzz
        //  -hem 3 hem 5 ile bolunebilen bir sayiya gelirse sayi yerine fizzBuzz yazdirin


        Scanner scan = new Scanner(System.in);
        System.out.print("Lutfen bir sayi giriniz : ");
        int userNum = scan.nextInt();

        for (int i = 1; i <userNum ; i++) {

            if(i%15 == 0)
                System.out.println("fizzBuzz");
            else if (i%3==0)
                System.out.println("fizz");
            else if (i%5 == 0)
                System.out.println("buzz");
            else
                System.out.println(i);
        }

    }
}
