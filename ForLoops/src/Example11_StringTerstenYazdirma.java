import java.util.Scanner;

public class Example11_StringTerstenYazdirma {
    public static void main(String[] args) {

        //10- Kullanıcıdan bır String isteyin ve String'i tersten yazdirin.

        Scanner scan = new Scanner(System.in);
        System.out.print("Lutfen bir metin giriniz : ");
        String userText = scan.nextLine();

        for (int i = userText.length()-1; i >=0 ; i--) {
            System.out.print(userText.charAt(i));
            
        }
    }
}
