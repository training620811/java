import java.util.Scanner;

public class Example12_StringTersineCevirme {
    public static void main(String[] args) {

        //12- Kullanıcıdan bır strıng ısteyın ve String'ı tersine cevirip kaydedın.

        Scanner scan = new Scanner(System.in);
        System.out.print("Lutfen bir metin giriniz : ");
        String userText = scan.nextLine();

        String reversedUserText ="";

        for (int i = userText.length()-1; i >=0 ; i--) {

            reversedUserText += userText.charAt(i);

        }
        System.out.println("Ters hali : "+reversedUserText);
    }
}
