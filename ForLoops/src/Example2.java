public class Example2 {
    public static void main(String[] args) {

        // 2- Verilen iki sayinin ilkinden baslayip ikinciye kadar
        //    3'er 3'er yazdirin. (İkinci sayi sartlari saglamiyorsa yazdirilmayabilir.)

        int first = 20;
        int last = 61;

        for (int i=first; i<=last; i+=3){

            System.out.print(i+ " ");
        }
    }
}
