import java.util.Scanner;

public class Example8_RakamlarToplami {
    public static void main(String[] args) {

        //7- Kullanicidan bir sayi aliniz.
        //   Alınan sayının rakamlar toplamını bulan bir method olusturunuz.
        //   Olusturulan methodu main'de cagırıp sonucu ekrana yazdırınız.

        Scanner scan= new Scanner(System.in);
        System.out.print("Lurfen bır sayı gırınız : ");
        int userNumInt= scan.nextInt();
        int rakamlarToplami= rakamlarToplaminiBulma(userNumInt);
        System.out.println(rakamlarToplami);
    }

    private static int rakamlarToplaminiBulma(int num) {
        int birlerBasamagi= 0;
        int rakamlarToplami = 0;
        String  userNumStr=""+num;   // sayıyı Stringe cevirdik.

        for (int i = 0; i <userNumStr.length() ; i++) {

            birlerBasamagi = num%10;
            rakamlarToplami+=birlerBasamagi;
            num = num/10;
        }
        return rakamlarToplami;
    }
}
