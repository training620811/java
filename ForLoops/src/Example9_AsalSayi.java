import java.util.Scanner;

public class Example9_AsalSayi {
    public static void main(String[] args) {

        //8- kullanıcının verdıgı bır sayının asal sayı olup olmadıgını bulun.

        Scanner scan = new Scanner(System.in);
        System.out.print("Lutfen sayinizi girini : ");
        int userNum = scan.nextInt();

        boolean primeNumber = false;

        for (int i = 2; i < userNum; i++) {

            if (userNum % i == 0) {
                primeNumber = true;
                break;
            }
        }
        if (primeNumber == true) System.out.println(userNum + " sayisi asal degildir.");
        else System.out.println(userNum +" sayisi asaldır." );
    }
}