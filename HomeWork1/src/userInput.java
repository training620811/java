import java.util.Scanner;

public class userInput {
    public static void main(String[] args) {
        /* Java'da kullanıcıdan değer almak istiyorsak;
            1- 'Scanner' objesi oluşturmamız gerekir.
            2- Olusturulan Scanner objesini kullanarak, kullanıcıdan bilgi alınır.
               Ve o bilgiyi oluşturulacak uygun (tanımlaması yapılmıs) bir veriable'a
               kaydetmek (değer olarak atamak) gerekir.
            Ör : Eğer kullanıcıdan isim almak istersek kullanacagımız veriable'ı String ile tanımlamak,
                 yaş bilgisi alacaksak atayacagımız veriable'ı byte,int,short,long gibi sayı tipinde
                 tanımlamak gerekir.

            NOT: Bir Class da 'Scanner' objesini bir defa olusturmak yeterlidir.!
         */

        // Ör: Kullanıcıdan isim almak istersek;

        //      1- Scanner' ı olusturuyoruz..
        Scanner scan= new Scanner(System.in);

        //      2- Kullanıcıdan talep edilen seyi soyluyoruz..
        System.out.println("Lütfen isminizi giriniz :");

        //      3- 'scan' isimli Scanner'ı kullanarak, bilgiyi uygun veriable'a atayip kullanıcıya input gonderiyoruz..
        String userName = scan.nextLine();

        //      4- userName degiskenimizi tekrar print ederek ekrana yazdırabiliriz. Ya da baska yerlerde bu degıskeni kullanabilriz.
        System.out.println("Girilen isim : " + userName);

        // Ör: Kullaniciden yas bilgisi almak istersek; yukarıdaki adımları bu sefer atanacak veriable'i int tipinde tanımlarız..;

        System.out.println("Lütfen yasinizi giriniz :");
        int userAge= scan.nextInt();
        System.out.println("Girilen yas : "+ userAge);

    }
}