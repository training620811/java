import java.util.Scanner;

public class celciusFahrenheigt {
    public static void main(String[] args) {
        //  2 - Hastanede kullanilacak bir programin bir parcasini siz yaziyorsunuz.
        //  Yazdiginiz kisim kullanicidan "celcius" olarak ates bilgisini aliyor ve ekrana "fahrenheigt" olarak ekrana yazdiriyor.
        //  Bu programi yaziniz.

        Scanner scan=new Scanner(System.in);

        System.out.println("Lutfen hastanin atesini giriniz : ");
        double celcius= scan.nextDouble();
        double fahrenheight;
        // °F = °C × 1.8 + 32
        fahrenheight = (celcius * 1.8) + 32;
        System.out.println(celcius+"°C = " + fahrenheight+"°F");

    }
}
