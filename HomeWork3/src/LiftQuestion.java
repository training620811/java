import java.util.Random;
import java.util.Scanner;

public class LiftQuestion {
    public static void main(String[] args) {

        /*
           4 - Bir asansore binecegim. Asansor kullanicin gidecegi kati girdi olarak aliyor.
               Kullanici -10 dan +10 kadar kat bilgisi girebilir. Asagidaki adimlari kolayin.

            - Asansorun var oldugu kat icin rastgele bir numara uretin (-10 ve +10 arasinda )
             (rastgele sayi uretmeyi internetten ogrenebilirsiniz),
            - Kullaniciya bir karsilama mesaji ile beraber gidecegi kat numarasini alin,
            - Aldiginiz kat numarasinin -10 ve +10 arasinda olup olmadigini ve
              bulundugu kat numarasini girip girmedigini kontrol edin.
              Eger sorun varsa "Gecersiz kat numarasi" diyip programdan cikin,
            - Eger kat numarasi ok ise "X katina geldiniz: diyin ve programdan cikin
        */

        // Oncelikle rastgele sayi uretmede iki farkli yol vardır.
        // İlki "random sınıf nesnesi" olusturup yapmak,
        // ikincisi ise "Math sınıfına" ait olan "random()" metodurur. Sadece (0.0 - 1.0) arasi double deger atar.
        // Eger math.random() kullanacaksak ve int deger istiyorsak cast etmek gerekir.
        // Eger Random nesnesi olusturarak yaparsak dogrudan istenilen tipte deger alabiliriz.
        // Eger iki sinir arasinda (sınırlar dahil) rastgele sayi alinacaksa;
        // rand.nextInt(Max-min+1)+min formulu kullanilir.

        Random rand = new Random();
        Scanner scan = new Scanner(System.in);

        int randLiftFloorNum = rand.nextInt((10-(-10))+1)+(-10);
        System.out.println("Asansorun bulundugu kat : " + randLiftFloorNum);

        System.out.println("Lutfen gitmek istediginiz kat numarasini giriniz :");
        int userFloorNum = scan.nextInt();

        if (userFloorNum>=-10 && userFloorNum<=10){
            if (userFloorNum!=randLiftFloorNum){
                System.out.println(userFloorNum + ". kata geldiniz.");
            }
            else{
                System.out.println("Zaten " + userFloorNum + ". kattasiniz.");
            }
        }
        else {
            System.out.println("Gecersiz kat numarasi girdiniz.");
        }

        /*
           Dusunme/yorum :
           Asansor ornegi de tipki isik acmak icin kullandigimiz anahtarlara dokunma gibidir.
           Yani cuz-i bir irade ile sadece tercihimizi belirtiyoruz. Gerisini yapma kudretimiz bulunmamaktadir.
           Sadece bizim dedigimizi yapmasinin hikmeti de bizim bana sormadan beni 3. ya da 5. kata cikardi
           dememize imkan birakmamasi ve tabiki de fayda sozkonusudur. Fakat asansor kanunun yani in cik sisteminin
           sadece talebi yerine getirmesi sozkonusu yoksa bizim taleplerimizin dogruluguna yanlisligina karisma kısmına
           girmemektedir. Ornegin 5. katta oturan birisi binadan cikmak icin eger bina cikis kapısı zemin kattaysa
           zemin katini asansorden talep etmelidir. Eger 1. katı talep ederse yanlis girdiniz demeyecektir.
           Talebi yerine getkirir. Kanunu isletir. Sonrasinda eger yanlis kata gidilirse karsiligi olarak kisiye bir kat daha
           inmek olacaktir. Kisacasi, kisinin yaptigi seyin bilincle yapmasinin gerekliligi, kisinin ozgur oldugu belki
           hikmetlerden bazilari olabilir.
           Iredesi ise cok kucuktur. Sadece talep eden konumunda. Otesiyle ilgili elinden gelecek bir sey yoktur.
           Kural kanun ne ise o isler. Burada insan dikkat etse tum faliyetlerinde sadece meyil gosteren sadece
           tercih eden pozisyonunda oldugunu cikarimlayabilir.
           Bir de isin tevekkul kismi olabilir. Asansorde sadece tercih yapilir gerisi -elinde de olmadigi icin- isleyise
           kalir. kisi -eger bilincli olarak dogru tercih yapma derdindeyse- sadece tercihinin dogru, yerinde olmasini
           umarak bekler.Tercihinden sonra ona kalan sadece beklemek ve tevekkul etmek kalmistir.
           Bir bakarki binadan cikmak icin asansorde yaptigi tercih onu zemin kata birakir.

           Deginmedigimiz bir nokta da asansorun kat sinirlarinin disinda tercihler kismi. Onda da sistem islemez
           islemediginde de sistem bozuk denir halbuki tercih bozuktur.


         */

    }
}
