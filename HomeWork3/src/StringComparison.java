import java.util.Scanner;

public class StringComparison {
    public static void main(String[] args) {

        // 3 - String Comparison: Bir cekilis yapiyoruz ama cekilisimizde hile var ne yazik ki.
        // Kullaniciya isim soruyoruz eger isim yunus ise "bravo kazandiniz" diye bir mesaj yaziyoruz,
        // degilse "Ne yazik ki kaybettiniz" diyoruz. (kullanici buyuk ya da kucuk harf girebilir)

        /*

         Java bir OOP (Nesne yonelimli programlama) dilidir.

         Java'da Stringleri karşılaştırmak için "==" (eşitlik operatörü) kullanamayiz
         çünkü " == (esitlik) " operatorleri stringlerin referansini,
         yani ayni nesne olup olmadiklarini karsilastirirlar.
         Sanki " == " operatorunun sol ve sağ tarafindaki nesnelerin ayni olup olmadigi karsilastiriliyormus gibi..

         Ama equals() metodu ise string ifadelerin referanslarini degil,
         referanslarin tuttugu degerleri karsilastirir.

         Toparlayacak olursak,
         Java’da " == operatörü " ile referanslarin/adreslerin (nesnelerin) esitlgi karsilastirilirken,
         " equals metodu " ile referanslarin/adreslerin/nesnelerin degerlerinin esitligi karsilastirilir.

         */

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen isminizi giriniz :");
        String userName = scan.nextLine();

        if (userName.equalsIgnoreCase("Yunus")){
            System.out.println("Bravo kazandiniz.");
        }
        else{
            System.out.println("Ne yazik ki kaybettiniz.");
        }
    }
}
