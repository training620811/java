public class WhatDoSyntaxAndSemanticsMean {
    public static void main(String[] args) {

        // 1 - Genel programlamada syntax ve semantics kelimeleri ne anlama gelir? Comment satiri olarak yaziniz.

        /*
            Her programlama dilindeki gecerli programlari yani kodlari belirleyen kurallar vardir.
            Bu kurallar ise "syntax (sozdizim)" ve "semantics (anlambilim)" olarak ikiye ayrilir.

            Yani yazilan programda kullanilan kodlar belirli bir syntax (sozdizim) kuralina gore dizilmeli ve
            kodlara belirli bir semantic (anlam yuklemesi) uygulanmalidir.

            Mesela,
            Java program dilinde her deyimin (anlamli ifadenin) sonunda noktali virgül bulunmasi
            syntax (sozdizim) kurallarina bir ornek,
            bir degiskenin kullanilmadan once tanimlanmasi ise bir semantic (anlambilim) kurali ornegidir.
         */
    }
}
