public class WhatDoesGarbageCollectionMeanInJava {
    public static void main(String[] args) {

        // 2 - Javada garbage collection ne demektir? Comment satiri olarak yaziniz.
        //     (Dileyen bu yapiyi, El Kuddus ismi ile de bagdastirip dusunebilir)

        /*

        Garbage Collection, otomatik bellek yonetim mekanizmasidir.
        Bu islemde önce heap bellege bakilir.
        Sonra kullanilan objelerin tespit edilmesi ve referans edilmeyenlerin silinmesi gerceklestirilir.
        Kullanilmayan/referans edilmeyen nesnelerin kapladigi alan bellekte bosa cıkarilir ve
        bellekte bos yer acilmis olur.

        Yani kullanılmayan nesnelerin yigin hafizasindan kaldirilmasi islemine
        "Garbage Collection (Cop Toplama)" denir.
        Java ise bu islemi otomatik cöp toplama yoluyla arkapalanda bizim icin yapar,
        'referans edilmeyen nesneleri' yok eder.

         */

        /*

        Evet garbage collection a bakinca insanin aklina en basitinden soyle seyler geliyor;
        Java bunu dusunmus, demis ki bellekte gereksiz, kullanilmayan, omru bitmis nesneleri ortadan kaldirayim
        yoksa bellegi bos yere dolu tutacak ve bu da programa yuk olacak.Bu cok mantikli bir is.
        Bir de bunu arkaplanda yapıyor. Bunu ogrenince dunyayi heap bellege benzetmemek elde degil. Omru bitmis
        canli/cansiz maddeleri dusunmemek elde degil. O omru bitmis kullanilmayacak olan cisimlerin dunyanin var olusundan
        beri surekli birikmedigini yani arkaplanda bir temizlik isinin yurudugunu gormemek elde degil.
        Sayet boyle olmasaydi dunya belleginde belki de suan bize yer olmazdi.
        Buradan da balta girmemis bakir ormanlardan tutun da denizin kilometrelerce derinligindeki atiklara kadar
        arka planda bir temizligin oldugunu anlamamak mumkun degil.
        Kısacasi dunyanin yaratilisindan beri bir temizleyici tarafindan "gercek sifir atik projesi" nin  isledigi asikar :)

         */



    }
}