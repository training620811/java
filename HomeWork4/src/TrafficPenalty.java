import java.util.Scanner;
public class TrafficPenalty {
    public static void main(String[] args) {

        /*
           2- Trafik memuru ceza yazarken hiz degerine gore para cezasi yazmakta.
              Asagidaki adimlara gore kod yaziniz:
            - Hiz degerini kullanicidan alalim
            - Hiz 100  ise ceza degeri 0 lira olsun
            - 150 ise ceza degeri 5 lira olsun
            - 200 ise ceza degeri 10 lira olsun
            Program girilen hiz degerine gore kullaniciya "X lira odenmeli" seklinde bir cikti yazsin
            Yukarindaki kodu yazarken switch statement kullanin ve sadece bir print satiri olsun.
         */

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen hiz degerini giriniz :");
        int speed = scan.nextInt();

        String penaltyValue = "";

            switch (speed) {
                case 100:
                    penaltyValue = "0 lira odenmeli";
                    break;
                case 150:
                    penaltyValue = "5 lira odenmeli";
                    break;
                case 200:
                    penaltyValue = "10 lira odenmeli";
                    break;
                default:                                    //Hocam default u bos birakmak istemedim.
                    penaltyValue = "200'un ustu hizlarda Ehliyete elkoyulur.";
            }
        System.out.println(penaltyValue);
    }
}
