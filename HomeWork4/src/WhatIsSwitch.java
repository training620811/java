public class WhatIsSwitch {
    public static void main(String[] args) {

        // 1 - Javada switch nedir? Ne ise yarar? Arastirip ornekle aciklayiniz.

        /*
           1- Bazı sorularda daha anlasilir kod ile calisma yapmak icin switch-case-default yapısı kullanilir.
           2- 'Switch' ifadesini 'Anahtar' ifadesi ile ozlestirebiliriz. Bir anahtar gorevi gorup,
           switch body'sinde ilgili degerin case'de karsiligini bulursa o case'e anahtar olarak giris yapar
           ve boylece ilgili degerin uyustugu case'deki kodlar aktif olur.
           3- switch statements'da switch parantezinde 'long,double,float ve boolean' kullanilmaz.
           4- switch parantezindeki yazilan degere uygun case calisir ve break gorunceye kadar calismaya devam eder.
           5- switch parantezindeki deger hic bir case ile uyusmazsa 'default' satiri devreye girer.
              (if - else if - else gibi)
           ÖR: Banka ATM'lerindeki 1-2-3-4-5-6... numarali islemlere yönlendirmek icin hazirlanan menulerde
               kullanilabilir.
         */
    }
}