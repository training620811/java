import java.util.Random;
import java.util.Scanner;

public class HocaninCozumu {

    public static void main(String[] args) {
        /**
         * Bir sayi tahmin oyunu yazacaksiniz. Kurallar ve adimlar su sekilde olmali:
         * 1 - 0-100 arasi bir rastgele sayi aklinizda tutacaksiniz.
         *     - Bunu yaparken seed degeri nedir ogrenin
         *     - Mevcut zaman degerini seed degeri olarak kullanin
         * 2 - Sonra kullaniciya 2 kere tahmin hakki veriyorsunuz.
         * 3 - Kullanici dogru tahmin ederse "Bravo bildiniz" seklinde bir mesaj yazarsiniz
         * 4 - Kullanici bilemezse "... kadar yaklastiniz" diye geri bildirimde bulunacaksiniz (her zaman pozitif bir uzaklik soyleyeceksiniz)
         * 5 - Kullanici -1 girdigi anda programiniz sonra erecek, o zamana kadar surekli yeni oyun baslayacak
         *
         * Odev1 - Yukaridaki soruyu iki for loop ile yapiniz (25 puan)
         * Odev2 - Yukaridaki soruyu iki while loop ile yapiniz (25 Puan)
         * Odev3 - Yukaridaki soruyu iki do-while loop ile yapiniz (25 Puan)
         * Odev4 - Yukaridaki soruyu bir for loop bir de do-while loop ile yapiniz (25 Puan)
         * @param args
         */

        for (; true; ) {

            Random random = new Random();
            random.setSeed(System.currentTimeMillis());
            int randomNumber = random.nextInt(101);
            System.out.println(randomNumber);

            boolean continueGame = true;
            for (int guessRight = 1; guessRight >= 0; guessRight--) {
                System.out.print("Sayi giriniz:");
                Scanner scanner = new Scanner(System.in);
                int userGuess = scanner.nextInt();
                if (userGuess == -1) {
                    continueGame = false;
                    break;
                }

                if (userGuess == randomNumber) {
                    System.out.println("Bravo");
                    break;
                } else {
                    if (guessRight == 1) {
                        System.out.println(
                                Math.abs(userGuess - randomNumber) +
                                        " kadar yaklastiniz." +
                                        "Tekrar tahmin ediniz.");
                    } else {
                        System.out.println("Kaybettiniz");
                    }
                }
            }

            if (!continueGame) {
                break;
            }
        }
    }
}
