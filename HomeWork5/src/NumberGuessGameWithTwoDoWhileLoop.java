import java.util.Random;
import java.util.Scanner;

public class NumberGuessGameWithTwoDoWhileLoop {
    public static void main(String[] args) {

        /*
                Bir sayi tahmin oyunu yazacaksiniz. Kurallar ve adimlar su sekilde olmali:
            1 - (0-100) arasi bir rastgele sayi aklinizda tutacaksiniz.
                - Bunu yaparken seed degeri nedir ogrenin
                - Mevcut zaman degerini seed degeri olarak kullanin
            2 - Sonra kullaniciya 2 kere tahmin hakki veriyorsunuz.
            3 - Kullanici dogru tahmin ederse "Bravo bildiniz" seklinde bir mesaj yazarsiniz
            4 - Kullanici bilemezse "... kadar yaklastiniz" diye geri bildirimde bulunacaksiniz
                (her zaman pozitif bir uzaklik soyleyeceksiniz)
            5 - Kullanici -1 girdigi anda programiniz sonra erecek, o zamana kadar surekli yeni oyun baslayacak

         */

        // Odev3 - Yukaridaki soruyu iki do-while loop ile yapiniz

        Scanner scan = new Scanner(System.in);

        boolean exitGameCheck = true;

        int i = 1;
        do {

            long seed = System.currentTimeMillis();
            Random rnd = new Random(seed);

            int randomNum = rnd.nextInt(101);

            System.out.println("------------------ " + i + ". Yeni Oyun ------------------");

//            System.out.println("Seed degeri : " + seed);                          // kontrol amaclıdır.
//            System.out.println("Rasgele olusturulan sayi : " + randomNum);       //  İhtiyac icin aktif edilebilir.

            String message = "";
            int j = 2;
            do {

                System.out.println("Mevcut " + j + " hakkınız vardır. Lutfen 0-100 arasi sayi tahmininizi giriniz. Cikis icin -1 giriniz :");
                int userNum = scan.nextInt();
                int differance = randomNum - userNum;

                if (userNum == -1) {
                    exitGameCheck = false;
                    message = "Cıkıs yaptınız.";
                    break;
                } else if (userNum != randomNum) {
                    if (differance > 0) {
                        System.out.println(differance + " kadar yaklastiniz.");
                    } else {
                        System.out.println(-1 * differance + " kadar yaklastiniz.");
                    }
                    message = "Hakkınız bıttı, uzgunuz kaybettınız";
                } else {
                    message = "Bravo bildiniz.";
                    break;
                }
                j--;
            } while (j >= 1);

            System.out.println(message);
            System.out.println("");

            i++;
        } while (i >= 0 && exitGameCheck);
    }
}
