import java.util.Random;
import java.util.Scanner;

public class NumberGuessGameWithTwoForLoop {
    public static void main(String[] args) {

        /*
                Bir sayi tahmin oyunu yazacaksiniz. Kurallar ve adimlar su sekilde olmali:
            1 - (0-100) arasi bir rastgele sayi aklinizda tutacaksiniz.
                - Bunu yaparken seed degeri nedir ogrenin
                - Mevcut zaman degerini seed degeri olarak kullanin
            2 - Sonra kullaniciya 2 kere tahmin hakki veriyorsunuz.
            3 - Kullanici dogru tahmin ederse "Bravo bildiniz" seklinde bir mesaj yazarsiniz
            4 - Kullanici bilemezse "... kadar yaklastiniz" diye geri bildirimde bulunacaksiniz
                (her zaman pozitif bir uzaklik soyleyeceksiniz)
            5 - Kullanici -1 girdigi anda programiniz sonra erecek, o zamana kadar surekli yeni oyun baslayacak
         */

        // Odev1 - Yukaridaki soruyu iki for loop ile yapiniz

        /*
         Seed Degeri : Random ile oluşturulan değerler Linear Congruential Generator (LCG) algoritması ile random değerler üretir.
         başlangıçta sabit bir değer alır. Bu değerin adı seed olarak tanımlanır.
         Programcı bu algoritmayı kullanmak isterse bu seed değerini uygulamanın açılış tarihi,
         system millisecond gibi değerler olarak alabilir.
         Random kütüphanesi Sistem saatini kullanarak bu seed değerini üretir.
         Bu da sistemin saatini bilen birinin daha kolay saldırmasına sebebiyet verebilir.
         */

        Scanner scan = new Scanner(System.in);

        boolean exitGameCheck = true;

        for (int i = 1; i >= 0 && exitGameCheck; i++) {

            long seed = System.currentTimeMillis();
            Random rnd = new Random(seed);

            int randomNum = rnd.nextInt(101);

            System.out.println("------------------ " + i + ". Yeni Oyun ------------------");

//            System.out.println("Seed degeri : " + seed);                          // Kontrol amaclıdır.
//            System.out.println("Rasgele olusturulan sayi : " + randomNum);       //  İhtiyac icin aktif edilebilir.

            String message = "";

            for (int j = 2; j >= 1; j--) {

                System.out.println("Mevcut " + j + " hakkınız vardır. Lutfen 0-100 arasi sayi tahmininizi giriniz. Cikis icin -1 giriniz :");
                int userNum = scan.nextInt();

                int differance = randomNum - userNum;

                if (userNum == -1) {
                    message = "Cıkıs yaptınız.";
                    exitGameCheck = false;
                    break;
                } else if (userNum != randomNum) {
                    if (differance > 0) {
                        System.out.println(differance + " kadar yaklastiniz.");
                    } else {
                        System.out.println(-1 * differance + " kadar yaklastiniz.");
                    }
                    message = "Hakkınız bıttı, uzgunuz kaybettınız";
                } else {
                    message = "Bravo bildiniz.";
                    break;
                }
            }
            System.out.println(message);
            System.out.println("");
        }
    }
}