import java.util.Scanner;

public class HocaninCozumu {
    /**
     * 1 - Asagidaki programlari yaziniz 50 puan):
     * <p>
     * Kullanicidan bir string aliyoruz.
     * O stringeki karakterlerden sadece sesli harfleri yazdiriyoruz
     * Stringdeki karakerlerdedn sadece sessiz harfleri yazdiriyoruz
     * Stringdeki karakterlerden sadece ilk sessiz harfi yazdirip sonra loopdan cikiyoruz
     * <p>
     * (Continue ve break kullanmanizi bekliyoruz)
     */
    public static void main(String[] args) {
        System.out.println("Bir kelime giriniz");
        Scanner scanner = new Scanner(System.in);
        String userData = scanner.next();
        for (char character : userData.toCharArray()) {
            if (sesliMi(character)) {
                System.out.println(character);
            }
        }

        System.out.println("=========================");
        for (char character : userData.toCharArray()) {
            if (sesliMi(character)) {
                continue;
            }
            System.out.println(character);
        }

        System.out.println("=========================");
        for (char character : userData.toCharArray()) {
            if (sesliMi(character)) {
                continue;
            } else {
                System.out.println(character);
                break;
            }

        }


    }

    public static boolean sesliMi(char ch) {
        char[] sesliler = {'a', 'e', 'i', 'o', 'u'};
        for (char c : sesliler) {
            if (ch == c) return true;
        }
        return false;
    }
}

