import java.util.Scanner;

public class UserStrWithSplitMethod {
    public static void main(String[] args) {

        // 2 -  Kullanicidan alinan bir cumleyi bosluklarina gore ayirip, icindeki kelimeleri yazdiran bir program yazdirin.

        //      Ornek:
        //      Kanepe kirildi, artik islevini yerine getirmediginden cope attim. Demek ki amacini yerine getirmeyen copu boyluyor.


        // split metodu : Bir String'i belirli bir karakter ya da karakter dızısıni
        // dikkate alarak parcalamaya yarar ve dızı (array) olarak verır.

        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen bir metin giriniz :");

        String userStr = scan.nextLine();

        String[] implementedSplitUserStr = userStr.split(" "); //Split sonucu array dondugu ıcın yeni bir degıskene
        // atama yaptım.

        for (int i = 0; i < implementedSplitUserStr.length; i++) {
            System.out.println(implementedSplitUserStr[i]);
        }
    }
}
