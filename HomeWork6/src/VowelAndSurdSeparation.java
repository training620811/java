import java.util.Arrays;
import java.util.Scanner;

public class VowelAndSurdSeparation {
    public static void main(String[] args) {

        // 1 - Asagidaki programlari yaziniz 50 puan):
        //
        //  Kullanicidan bir string aliyoruz.
        //  O stringeki karakterlerden sadece sesli harfleri yazdiriyoruz
        //  Stringdeki karakerlerdedn sadece sessiz harfleri yazdiriyoruz
        //  Stringdeki karakterlerden sadece ilk sessiz harfi yazdirip sonra loopdan cikiyoruz
        //
        //  (Continue ve break kullanmanizi bekliyoruz)


        char[] vowelLetter = {'a', 'e', 'o', 'ö', 'u', 'ü', 'i', 'ı'};
        char[] surdLetter = {'b', 'c', 'ç', 'd', 'f', 'g', 'ğ', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 'ş', 't', 'v', 'y', 'z', 'x', 'w', 'q'};
        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen bir metin giriniz :");
        String userStr = scan.nextLine();

        String vowelLetterList = "";
        String surdLetterList = "";
        String firstSurdLetter = "";

        for (int i = 0; i < userStr.length(); i++) {

            for (int j = 0; j < vowelLetter.length; j++) {
                if (userStr.toLowerCase().charAt(i) != vowelLetter[j]) {
                    continue;
                }
                vowelLetterList += userStr.charAt(i);
            }

            for (int j = 0; j < surdLetter.length; j++) {
                if (userStr.toLowerCase().charAt(i) != surdLetter[j]) {
                    continue;
                }
                surdLetterList += userStr.charAt(i);
            }
        }
        boolean exit = true;
        for (int i = 0; i < userStr.length() && exit; i++) {
            for (int j = 0; j < surdLetter.length; j++) {
                if (userStr.toLowerCase().charAt(i) == surdLetter[j]) {
                    firstSurdLetter += userStr.charAt(i);
                    exit = false;
                    break;
                }
            }
        }
        System.out.println("Sesli harfler : " + Arrays.toString(vowelLetterList.toCharArray()));
        System.out.println("Sessiz harfler : " + Arrays.toString(surdLetterList.toCharArray()));
        System.out.println("İlk sessiz harf : " + Arrays.toString(firstSurdLetter.toCharArray()));
    }
}