import java.util.Arrays;
import java.util.Random;

public class AnnualSituationAnalysisWithMethods {

    /*
    Yillik Durum
    Sirketteki yillik durumun raporlanmasi icin yoneticiler bizden guzel bir yazilim istiyorlar.
    Asagidaki ozelliklerde bir program yaziniz:
    1 - Gelir listesi: 12 ayin gelir durumunu iceren bir liste tanimlatip
        her bir ay icin 100-1000 arasinda olacak sekilde rastgele gelir degeri tanimlayiniz. (10 puan)
    2 - Gelir listesini gosterme: Bir metod yaziniz parametre olarak listeyi alsin,
        dongu ile listeyi gezip kacinci ayda ne kadar gelir elde ettiginizi ekrana yazdirin.
        Sonra o metodunuzu cagirip ekrana her ayin gelir durumunu yazdirin. (10 puan)
    3 - En cok gelir getiren ay: Bir method yaziniz, parametre olarak yillik gelir listesini alsin,
        dongu ile listeyi gezip en cok gelir getiren ay hangisi ise onun numarasini bulsun ve return etsin.
        Ana programinizda bu metodu cagirarak en cok gelir getiren ay hangisi ise onun numarasini ekrana yazdirin. (10 puan)
    4 - Gider: Yukaridakilerin aynisini gider icin de yapin. (10+10+10 puan)
    5 - Kar: kar = gelir - gider olacak sekilde yukaridakilerin aynisini kar icin de yapin. (rastgelelik yok) (10 puan)
    6 - Ciro: Karin %30 u vergi olarak odenecek.
        Once bir metod yazin, o metod parametre olarak sirketin kar listesini alsin,
        yillik toplam kar ne ise loop ile hesaplasin sonra buldugu toplami return etsin.
        Baska bir metod daha yazin, o da toplam kari ve vergi miktarini (yuzde olarak) parametre olarak alsin,
        kardan vergiyi dussun, yani ciroyu hesaplasin ve return etsin. Metod(lari) cagirip ekrana yillik ciroyu yazdirin.(10+10 puan)
    7 - Sirketin yillik cirosunu hesapladiniz. Simdi de sirketin durumuna bakalim.
        Asagidaki sartlara gore sirketin durumunu ekrana yazdiran bir metod yazin:
        - 250 altinda Iflas
        - 250-500 arasinda risk altinda
        - 500-750 arasinda ortalama
        - 750-100 iyi
        (10 puan)
     */
    public static void main(String[] args) {

        Random random = new Random();
        random.setSeed(System.currentTimeMillis());

        System.out.println("");
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("");

        // 1 - Gelir listesi olusturma :

        int[] yillikGelirListesi = new int[12];

        for (int i = 0; i < yillikGelirListesi.length; i++) {

            int aylikGelir = random.nextInt(1000 - 100 + 1) + 100;
            yillikGelirListesi[i] = aylikGelir;
        }

        // 2 - Gelir listesini gosterme :

        yillikGelirListesiniGosterme(yillikGelirListesi);
        System.out.println("");

        // 3 - En cok gelir getiren ay :

        int maxGelirliAy = enCokGelirGetirenAy(yillikGelirListesi);
        System.out.println("En cok gelir getiren ay : " + maxGelirliAy + ". ay");
        System.out.println("");

        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("");

        // 4a - Gider listesi olusturma :

        int[] yillikGiderListesi = new int[12];

        for (int i = 0; i < yillikGiderListesi.length; i++) {

            int aylikGider = random.nextInt(1000 - 100 + 1) + 100;
            yillikGiderListesi[i] = aylikGider;
        }

        // 4b - Gider listesini gosterme :

        yillikGiderListesiniGosterme(yillikGiderListesi);
        System.out.println("");

        // 4c - En cok gider olan ay :

        int maxGiderliAy = enCokGiderOlanAy(yillikGiderListesi);
        System.out.println("En cok gider olan ay : " + maxGiderliAy + ". ay");
        System.out.println("");

        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("");

        // 5a - Kar listesi olusturma :

        int[] yillikKarZararListesi = new int[12];

        for (int i = 0; i < yillikKarZararListesi.length; i++) {
            yillikKarZararListesi[i] = yillikGelirListesi[i] - yillikGiderListesi[i];
        }

        // 5b - Yillik kar/zarar listesini gosterme :

        yillikKarZararListesiniGosterme(yillikKarZararListesi);
        System.out.println("");

        // 5c - En cok kar olan ay :

        int maxKarliAy = enCokKarOlanAy(yillikKarZararListesi);
        System.out.println("En cok kar olan ay : " + maxKarliAy + ". ay");
        System.out.println("");

        System.out.println("------------------------------------------------------------------------------------");

        // 6a - Yillik toplam kar/zarar :

        int toplamKar = toplamKarZarar(yillikKarZararListesi);
        System.out.println("Yillik toplam kar/zarar : " + toplamKar);
        System.out.println("");

        // 6b - Yillik ciro :

        double vergi = 0.30;
        double ciroYillik = yillikCiro(toplamKar, vergi);
        System.out.println("Sirketin yillik cirosu : " + ciroYillik);
        System.out.println("");

        // 7 - Sirketin durumu :

        String sirketinSonDurumu = sirketDurumu(ciroYillik);
        System.out.println("Sirketin durumu : " + sirketinSonDurumu);
    }

    // 2
    public static void yillikGelirListesiniGosterme(int[] yillikGelir) {

        System.out.println("Yıllık Gelir Listesi : " + Arrays.toString(yillikGelir));
        System.out.println("");

        for (int i = 0; i < yillikGelir.length; i++) {

            System.out.println((i + 1) + ". ay : " + yillikGelir[i]);
        }
    }

    // 3
    public static int enCokGelirGetirenAy(int[] yillikListeGelir) {

        int enCokGelir = yillikListeGelir[0];

        for (int gelir : yillikListeGelir) {

            if (gelir > enCokGelir) {
                enCokGelir = gelir;
            }
        }

        int index = 0;

        for (int i = 0; i < yillikListeGelir.length; i++) {
            if (yillikListeGelir[i] == enCokGelir) {
                index = i;
                break;
            }
        }

        int enCokGelirliAy = index + 1;

        return enCokGelirliAy;
    }     // Sorudaki istege gore sadece ilgili ay return edildi.
                                                                            // Miktar consola yansıtılmadı.

    // 4b
    public static void yillikGiderListesiniGosterme(int[] yillikGider) {

        System.out.println("Yillik Gider Listesi : " + Arrays.toString(yillikGider));
        System.out.println("");

        for (int i = 0; i < yillikGider.length; i++) {

            System.out.println((i + 1) + ". ay : " + yillikGider[i]);
        }
    }

    // 4c
    public static int enCokGiderOlanAy(int[] yillikListeGider) {

        int enCokGider = yillikListeGider[0];

        for (int gider : yillikListeGider) {
            if (gider > enCokGider) {
                enCokGider = gider;
            }
        }

        int index = 0;

        for (int i = 0; i < yillikListeGider.length; i++) {
            if (yillikListeGider[i] == enCokGider) {
                index = i;
                break;
            }
        }

        int enCokGiderAy = index + 1;

        return enCokGiderAy;
    }    // Sorudaki istege gore sadece ilgili ay return edildi.
                                                                        // Miktar consola yansıtılmadı.

    // 5b
    public static void yillikKarZararListesiniGosterme(int[] yillikKarZarar) {

        System.out.println("Yillik Kar/Zarar Listesi : " + Arrays.toString(yillikKarZarar));
        System.out.println("");

        for (int i = 0; i < yillikKarZarar.length; i++) {

            System.out.println((i + 1) + ". ay : " + yillikKarZarar[i]);
        }
    }

    // 5c
    public static int enCokKarOlanAy(int[] yillikListeKarZarar) {

        int enCokKar = yillikListeKarZarar[0];

        for (int kar : yillikListeKarZarar) {

            if (kar > enCokKar) {
                enCokKar = kar;
            }
        }

        int index = 0;

        for (int i = 0; i < yillikListeKarZarar.length; i++) {
            if (yillikListeKarZarar[i] == enCokKar) {
                index = i;
                break;
            }
        }

        int enCokKarAy = index + 1;

        return enCokKarAy;
    }   // Sorudaki istege gore sadece ilgili ay return edildi.
                                                                        // Miktar consola yansıtılmadı.

    // 6a
    public static int toplamKarZarar(int[] yillikListeKarZarar) {

        int toplamKarZarar = 0;

        for (int kar : yillikListeKarZarar) {
            toplamKarZarar += kar;
        }
        return toplamKarZarar;
    }

    // 6b
    public static double yillikCiro(int toplamKar, double vergiMiktari) {

        if (toplamKar > 0) {
            double ciro = toplamKar - (toplamKar * vergiMiktari);
            return ciro;
        } else {
            double ciro = toplamKar;
            return ciro;
        }
    }

    // 7
    public static String sirketDurumu(double ciro) {

        String durum = "";

        if (ciro < 250) {
            durum = "Iflas";
        } else if (ciro < 500) {
            durum = "Risk altında";
        } else if (ciro < 750) {
            durum = "Ortalama";
        } else if (ciro < 1000) {
            durum = "Iyi";
        } else {
            durum = "Cok iyi";
        }
        return durum;
    }
}