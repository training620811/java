/*
- Kitaplari temsilen bir Kitap sinifiniz olacak. Her kitabin asagidaki ozellikleri olacak:
    - ISBN
    - Kitap Adi
    - Yazari
    - basim yili
    - Kitap cesidi - hikaye, roman, makale, din, felsefe gibi
 */
public class Book {

    private String ISBN;
    private String kitapAdi;
    private String kitapYazari;
    private String kitapBasimYili;
    private String kitapCesidi;

    public Book(String ISBN, String kitapAdi, String kitapYazari, String kitapBasimYili, String kitapCesidi) {
        this.ISBN = ISBN;
        this.kitapAdi = kitapAdi;
        this.kitapYazari = kitapYazari;
        this.kitapBasimYili = kitapBasimYili;
        this.kitapCesidi = kitapCesidi;
    }

    public String getISBN() {
        return ISBN;
    }

    @Override
    public String toString() {
        return
                "ISBN= " + ISBN + "\n"+
                "kitapAdi= " + kitapAdi +"\n"+
                "kitapYazari= " + kitapYazari +"\n"+
                "kitapBasimYili= " + kitapBasimYili +"\n"+
                "kitapCesidi= " + kitapCesidi+ "\n" ;
    }
}
