/*
- Kutuphane sinifiniz olacak ve asagidaki ozellik ve fonsiyonlara sahip olacak:
    - adi
    - adresi
    - bir kitap listesine
    - kitap listesini getirme
    - kitap ekleme
    - kitap cikarma
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Library {

    private String kutuphaneAdi;
    private String kutuphaneAdresi;
    private ArrayList<Book> kitapListesi ;

    Scanner scan = new Scanner(System.in);

    public Library() {
        kutuphaneAdi = "CodingBook Library";
        kutuphaneAdresi = "Everywhere";
        kitapListesi = new ArrayList<>();
    }

    public void addBook() {

        while (true) {
            System.out.println("Eklemek istediginiz kitap bilgilerini giriniz. (Cikis icin bos giris)");


            System.out.print("ISBN No: ");
            String isbnNo = scan.nextLine();
            if (isbnNo.equals("")) {
                break;
            }
            System.out.print("Kitap Adi: ");
            String kitapAdi = scan.nextLine();
            System.out.print("Kitap Yazari: ");
            String kitapYazari = scan.nextLine();
            System.out.print("Kitap Basim Yili: ");
            String kitapBasimYili = scan.nextLine();
//            scan.nextLine();                          // Run sırasında satır atlaması yasanırsa aktıf hale getırılebılınır.!
            System.out.print("Kitap Cesidi: ");
            String kitapCesidi = scan.nextLine();

            Book kitap = new Book(isbnNo, kitapAdi, kitapYazari, kitapBasimYili, kitapCesidi);

            kitapListesi.add(kitap);

            System.out.println("");
        }
    }

    public void removeBook() {
        while (true) {
            System.out.println("Cıkarmak istediginiz kitabin ISBN numarasini giriniz. (Cikis icin bos giris)");
            String cikacakKitabinIsbnNum = scan.nextLine();

            if (cikacakKitabinIsbnNum.equals("")) {
                break;
            }

            for (Book kitap : kitapListesi) {
                if (kitap.getISBN().equals(cikacakKitabinIsbnNum)) {
                    kitapListesi.remove(kitap);
                    break;
                }
            }
        }
    }

    public void kitapListeleme() {
        if(kitapListesi.isEmpty()){
            System.out.println("Kitap lisetesinde henuz kitap bulunmamaktadir. Lutfen once kitap ekleyiniz.\n");
        }
        else{
            for (Book kitap : kitapListesi) {
                System.out.println(kitap);
            }
        }
    }

    @Override
    public String toString() {
        return "Kutuphane Adi = " + kutuphaneAdi + "\n" + "Kutuphane Adresi = " + kutuphaneAdresi;
    }

}
