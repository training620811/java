/*
- Ana programinizda asagidaki komutlarla islem yaptirabilecegim:
    - l : kutuphanedeki kitaplari listeleme - list
    - a : kitap ekleme - add - (eklerken kullanicidan kitabin detaylarini istemeyi unutmayin)
    - r : kitap cikarma - remove - (cikarirken cikaracaginiz kitabin ISBN numarasini istemeyi unutmayin)
 */

import java.util.Scanner;

public class MainProgram {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Library kutuphane = new Library();

        while (true) {

            System.out.println("Kutuphanedeki kitaplarini listelemek icin: l");
            System.out.println("kitap eklemek icin: a");
            System.out.println("Kitap cikarmak icin: r");
            System.out.println("Cikis yapmak icin: q");
            System.out.println("");
            System.out.println("Lutfen yukaridaki komutlardan birini giriniz: ");

            String kullaniciTercihi = scan.nextLine();

            if (kullaniciTercihi.equalsIgnoreCase("q")) {
                System.out.println("Cikis yaptiniz.");
                break;
            } else {
                switch (kullaniciTercihi) {
                    case "l":
                        System.out.println(kutuphane);
                        System.out.println("");
                        kutuphane.kitapListeleme();
                        break;
                    case "a":
                        kutuphane.addBook();
                        break;
                    case "r":
                        kutuphane.removeBook();
                        break;
                    default:
                        System.out.println("Gecersiz giris. Tekrar giris yapiniz.");
                        break;
                }
            }
        }
    }
}