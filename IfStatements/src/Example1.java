import java.util.Scanner;

public class Example1 {
    public static void main(String[] args) {

        // 1- Kullanıcıdan bir sayi isteyin, sayiyi kontrol edip 5 ile bolunebiliyorsa
        //    "Sayi 5'in tam kati" yazdirin.

        Scanner scan=new Scanner(System.in);

        System.out.println("Lutfen bir sayi giriniz :");
        int userNum= scan.nextInt();
         if (userNum%5==0){
             System.out.println(userNum + " sayisi 5'in tam katıdır.");
         }else{
             System.out.println(userNum + " sayisi 5'in tam kati degildir.");
         }


    }
}