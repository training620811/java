import java.util.Scanner;

public class Example10 {
    public static void main(String[] args) {

        // 10- Kullanicidan iki sayi alin ve buyuk olmayan sayiyi yazdirin.

        Scanner scan=new Scanner(System.in);

        System.out.println("Lutfen ilk sayiyi giriniz :");
        int userNum1=scan.nextInt();

        System.out.println("Lutfen ikinci sayiyi giriniz :");
        int userNum2=scan.nextInt();

        if (userNum1==userNum2){
            System.out.println(userNum1 + " sayisi " + userNum2 + " sayisina esittir.");
        }else if (userNum1>userNum2){
            System.out.println("Kucuk sayi : " + userNum2);
        }else{
            System.out.println("Kucuk sayi :" + userNum1);
        }
    }
}
