import java.util.Scanner;

public class Example11 {
    public static void main(String[] args) {

        // 11- Kullanicidan bir sayi alin ve mutlak degerini yazdirin.

        Scanner scan=new Scanner(System.in);

        System.out.println("Lutfen bir sayi giriniz :");
        int userNum=scan.nextInt();
        int numAbsoluteValue;

        if (userNum<0){
            numAbsoluteValue = -1 * userNum;
            System.out.println(userNum + " sayisinin mutlak degeri : " + numAbsoluteValue);
        }else{
            numAbsoluteValue = userNum;
            System.out.println(userNum + " sayisinin mutlak degeri : " + numAbsoluteValue);
        }

    }
}
