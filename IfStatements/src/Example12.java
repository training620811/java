import java.util.Scanner;

public class Example12 {
    public static void main(String[] args) {

        // 12- Kullanicidan yas ve cinsiyet bilgisi alip asketlik icin uygun olup olmadigini
        //     kontrol edeniz. (Askerlik yasi en az 20 dir.)

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen cinsiyetinizi E ya da K seklinde giriniz :");
        char cinsiyet = scan.next().charAt(0);

        System.out.println("Lutfen yasinizi girini :");
        int userAge = scan.nextInt();

        if (cinsiyet == 'K' || cinsiyet == 'k') {
            System.out.println("Askere gidemezsiniz.");
        } else if (cinsiyet == 'E' || cinsiyet == 'e') {
            if (userAge >= 20) {
                System.out.println("Askere gidebilirsiniz.");
            } else {
                System.out.println("Askere gitmek icin " + (20 - userAge) + " yıl beklemelisiniz.");
            }
        } else {
            System.out.println("Cinsiyet bilgisini yanlis girdiniz.");
        }
    }
}
