import java.util.Scanner;

public class Example13 {
    public static void main(String[] args) {

        // 13- Kullanicidan bir tam sayi alin.
        //     Sayi pozitifse, cift sayi veya cift sayi degil seceneklerinden uygun olani yazin.
        //     Sayi pozitif degilse, 3 basamakli veya 3 basamakli degil seceneklerinden uygun olani yazin.

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen bir sayi giriniz :");
        double userNum = scan.nextDouble();

        if (userNum<0){
            if (userNum>-1000 && userNum<-99){
                System.out.println(userNum + " sayisi 3 basamaklidir.");
            }else{
                System.out.println(userNum + " sayisi 3 basamakli degildir.");
            }
        }else{
            if (userNum%2==0){
                System.out.println(userNum + " sayisi cift sayidir.");
            }else {
                System.out.println(userNum + " sayisi cift sayi degildir.");
            }
        }
    }
}
