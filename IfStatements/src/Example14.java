import java.util.Scanner;

public class Example14 {
    public static void main(String[] args) {

        // 14- Kullanıcıdan 3 sayi alın ve en buyuk olanı yazdırın.

        Scanner scan= new Scanner(System.in);

        System.out.println("Lutfen ilk sayiyi giriniz :");
        int num1= scan.nextInt();
        System.out.println("Lutfen ikinci sayiyi giriniz :");
        int num2 =scan.nextInt();
        System.out.println("Lutfen ucuncu sayiyi giriniz :");
        int num3 = scan.nextInt();
        int biggestNum = num1;

        if (biggestNum<num2){
            biggestNum=num2;
        }
        if (biggestNum<num3){
            biggestNum=num3;
        }

        System.out.println("En buyuk sayi = " + biggestNum);
    }
}
