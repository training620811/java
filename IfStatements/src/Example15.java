import java.util.Scanner;

public class Example15 {
    public static void main(String[] args) {

        // 15- Kullanicidan gun numarasini alip, gun ismini yazdirin.

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen gun numarasini giriniz :");
        int dayNum= scan.nextInt();

        if (dayNum==1) System.out.println("Pazartesi");
        else if (dayNum==2) System.out.println("Sali");
        else if (dayNum==3) System.out.println("Carsamba");
        else if (dayNum==4) System.out.println("Persembe");
        else if (dayNum==5) System.out.println("Cuma");
        else if (dayNum==6) System.out.println("Cumartesi");
        else if (dayNum==7) System.out.println("Pazar");
        else System.out.println("Gecersiz gun numarasi");

        }
    }
