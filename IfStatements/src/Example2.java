import java.util.Scanner;

public class Example2 {
    public static void main(String[] args) {

        // 2- Kullanıcıdan bir harf alin, harf ile baslauan bir ay varsa yazdirin.
        //    Ör: Kullanıcı o veya O yazdiginda output Ocak olsun.

        Scanner scan= new Scanner(System.in);
        System.out.println("Lutfen bir harf giriniz :");
        char firstLetter=scan.next().charAt(0);
        System.out.println("Girilen harf : "+ firstLetter);

        if (firstLetter=='o' || firstLetter=='O'){
            System.out.println("Ocak");
        }else if (firstLetter=='s' || firstLetter=='S'){
            System.out.println("Subat");
        }else if (firstLetter=='m' || firstLetter=='M'){
            System.out.println("Mart ya da Mayıs");
        }else if (firstLetter=='n' || firstLetter=='N'){
            System.out.println("Nisan");
        }else if (firstLetter=='h' || firstLetter=='H'){
            System.out.println("Haziran");
        }else if (firstLetter=='t' || firstLetter=='T'){
            System.out.println("Temmuz");
        }else if (firstLetter=='a' || firstLetter=='A'){
            System.out.println("Agustos ya da Aralik");
        }else if (firstLetter=='k' || firstLetter=='K'){
            System.out.println("Kasim");
        }else {
            System.out.println("Girilen harf ile baslayan bir ay yoktur.");
        }
    }
}
