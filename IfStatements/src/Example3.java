import java.util.Scanner;

public class Example3 {
    public static void main(String[] args) {

        // 3- Kullanicidan bir sayı alin, sayi 3 ile bolunuyorsa "Uc ile bolunebilen sayi",
        //    5 ile bolunebiliyorsa  "Bes ile bolunebilen sayi",
        //    hem 3 e hem 5 e bolunurse "Super sayi" yazdirin.

        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen bir sayi giriniz :");
        int userNum = scan.nextInt();

        /*
            if cumlesinden sonra {} kullanılırsa if sartı saglandıgında
            {} ici tamamen calıstırılır, saglanmazsa ici hic calistirilmaz.
            Ancak {} kullanılmazsa, java if body'si olarak if sartindan sonra ilk ; e kadar
            olan kismi alir geriye kalan kodlar if cumlesi iligli olmaz. Tek satirlik islem bile olsa
            {} kullanarak yapmak onemli bir tavsiyedir.
         */
            if ((userNum % 3 == 0) && (userNum % 5 == 0)) {
                System.out.println(userNum + " sayisi super sayidir.");
            } else if (userNum % 3 == 0) {
                System.out.println(userNum + " sayisi 3 ile bolunebilen bir sayidir.");
            } else if (userNum % 5 == 0) {
                System.out.println(userNum + " sayisi 5 ile bolunebilen bir sayidir.");
            } else {
                System.out.println(userNum + " sayisi 5'e ya da 3'e tam bolunmez.");
            }
        }
    }

