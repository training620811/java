import java.util.Scanner;

public class Example4 {
    public static void main(String[] args) {

        // 4- Kullanicidan bir ucgenin 3 kenar uzunlugunu alin, ucgen eskenar ise
        //    "Eskenar ucgen" yazdirin.

        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen uckenin birinci kenarini giriniz :");
        double  first= scan.nextInt();
        System.out.println("Lutfen uckenin ikinci kenarini giriniz :");
        double second= scan.nextInt();
        System.out.println("Lutfen uckenin ucuncu kenarini giriniz :");
        double third = scan.nextInt();

        if ((first==second) && (second==third)){
            System.out.println("Eskenar ucgen");
        }else if ((first==second) || (first==third) || (second==third)){
            System.out.println("Ikiz kenar ucgen");
        }else{
            System.out.println("Eskener ucgen ya da ikiz kenar ucgen degildir.");
        }
    }
}
