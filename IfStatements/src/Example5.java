import java.util.Scanner;

public class Example5 {
    public static void main(String[] args) {

        // 5- Kullanicidan notunu alin 50 veya buyukse "Sinifi gectin",
        //    50'den kucukse "Maalesef kaldin" yazdirin.

        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen notu giriniz :");
        int note = scan.nextInt();

        if ((note >= 0) && (note <= 100)) {

            if (note >= 50) {
                System.out.println("Sinifi gectin.");
            } else {
                System.out.println("Maalesef kaldin.");
            }
        }else{
            System.out.println("Lutfen gecerli bir not giriniz.");
        }
    }
}
