import java.util.Scanner;

public class Example6 {
    public static void main(String[] args) {

        /* 6- Kullanicidan cinsiyetini ve yasini isteyin,
            Kadın, 60 yas ve uzeri,
            Erkek 65 yas ve uzeri ise emekli olabilir,
            Cinsiyet ve yasini dikkate alarak "Emekli olabilirsin" ya da
            "Emekli olmak icin... yıl daha calisman gerekir" yazdirin.
         */

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen cinsiyetinizi giriniz :");
        String cinsiyet = scan.nextLine();

        System.out.println("Lutfen yasinizi giriniz :");
        int userAge = scan.nextInt();

        // equalsIgnoreCase => buyuk kucuk ayrimi yapmaz esitlik sorgular.

        if (cinsiyet.equalsIgnoreCase("Kadin")) {
            if (userAge>=0 && userAge< 60) {
                System.out.println("Emekli olabilmeniz icin " + (60 - userAge) + " yil daha calismalisiniz.");
            } else if (userAge>=60){
                System.out.println("Emekli olabilirsiniz.");
            }else{
                System.out.println("Yas bilgisi hatali");
            }
        } else if (cinsiyet.equalsIgnoreCase("Erkek")) {
            if (userAge>=0 && userAge<65){
                System.out.println("Emekli olabilmeniz icin " + (65 - userAge) + " yil daha calismalisiniz.");
            }else if (userAge >= 65) {
                System.out.println("Emekli olabilirsin.");
            } else {
                System.out.println("Yas bilgisi hatali");
            }
        } else{
            System.out.println("Cinsiyet bilgisi hatali");
        }
    }
}
