import java.util.Scanner;

public class Example7 {
    public static void main(String[] args) {

        // 7- Kullanicidan bir karakter girmesini isteyin, girilen karakterin buyuk harf olup
        //    olmadigini yazdirin.

        Scanner scan=new Scanner(System.in);
        System.out.println("Lutfen bir karakter giriniz :");
        char userChar=scan.nextLine().charAt(0);
        System.out.println("Girilen harf : " + userChar);

        // Matematiksel operatorlerde charlarin sayisal ascii karsiliklari ile islem yapilir.

        if (userChar>='A' && userChar<='Z'){
            System.out.println(userChar + " buyuk harftir.");
        }else{
            System.out.println(userChar + " buyuk harf degildir.");
        }
    }
}
