import java.util.Scanner;

public class Example8 {
    public static void main(String[] args) {

        // 8- Kullanicidan bir harf isteyin, girilen karakter kucuk harf ise
        //    onu buyuk harf olarak yazdırın, yoksa girilen harfi yazdirin.

        Scanner scan=new Scanner(System.in);
        System.out.println("Lutfen bir harf giriniz :");
        char letter=scan.next().charAt(0);

        // Matematiksel operatorlerde charlarin sayisal ascii karsiliklari ile islem yapilir.
        // ascii tablosunda buyuk harfler kucuk harflerden 32 eksiktir.

        if(letter>='a' && letter<='z'){
            System.out.println((char)(letter-32));
        }else{
            System.out.println(letter);
        }
    }
}
