import java.util.Scanner;

public class Example9 {
    public static void main(String[] args) {

        // 9- Kullanicinin kilo(kg) ve boyunu(cm) isteyip vucut kitle endeksini hesaplayin.
        //    Vucut kitle endeksi = (kilo*10000/(boy*boy))
        //    vucut kitle endeksi 30'dan buyukse obez,
        //    25-30 arasi ise kilolu,
        //    20-25 arasi ise normal,
        //    20'den kucukse zayif yazdirin.

        Scanner scan=new Scanner(System.in);
        System.out.println("Lutfen agirliginizi kilo olarak giriniz :");
        double  userWeight=scan.nextDouble();

        System.out.println("Lutfen buyunuzu cm olarak giriniz :");
        double userLength=scan.nextDouble();

        double vke=(userWeight*10000)/(userLength*userLength);
        System.out.println("Vucut kitle endeksiniz : " + vke);

        if (vke>30) System.out.println("Obez");         // If cumlelerinden sonra tek satir
        else if(vke>25) System.out.println("Kilolu");   // kullandigimiz icin {} kullanmadik.
        else if(vke>=20) System.out.println("Normal");  // İstege baglidir.
        else System.out.println("Zayif");

    }
}
