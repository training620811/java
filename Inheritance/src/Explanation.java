public class Explanation {

    /** Inheritance - Kalıtım - Miras
     *
     * Bir obje baska objelerden olusuyorsa veya sahipse, kalıtım (inheritance) kullanılmamalıdır.
     * Iki class arasında inheritance iliskisi tespit edebilmek icin (is a) classlar arasında sorusu sorulabilir. Engine is a Part ?
     * Inheritance kullanılırken, SRP (Single Responsibility Principle) ilkesinin dogrulugundan emin olunmalıdır.
     * Interit etmenin bir class'a birden fazla sorumluluk yukledıgı fark edildiginde superclass ın birden fazla subclass'ı olusturulmali
     */

    /**
     * Polymorphisim - Cok boyutluluk - Cok formluluk
     *
     * Aynı ısımdekı methodu farklı farklı sonuclar verecek sekilde calıstırmak ıcın override ya da overload yontemi kullanılır.
     * Polymorphisimin gerceklestirilmesi 2 sekilde yapılabilir.
     * 1-Bir class icin de overload ile yapılabilir ya da
     * 2-Inheritance kullanılarak super ve subclass'lar ıcerısınde Data turu ve constructor farklı kullanılarak override edilebilir.
     * Bir subclass superclass'ından ınherit ettigi methodları override ederek kendisine ozgu uyarlayabilir.
     * Objeler polymorfiktir, yani bir cok farklı degısken turunde (Data type) kullanılabilir, olusturulabilirler.
     * Yurutulen methodlar her zaman objenın gercek turuyle ilgilidir. Bu duruma polymorphisim denir.
     */

    /** Abstraction - Soyutlama
     * Data (veri) soyutlama, belirli ayrıntıları gizleme ve kullanıcıya yalnızca gerekli bilgileri gosterme islemidir.
     * Abstraction, abstract class'lar ya da interface'ler ile gerceklestirilir.
     *
     * 1- Abstract Class - Soyut class
     *
     * Inheritance hiyerarsisinde yeri olan fakat kendi basına bir obje(nesne) olması uygun olmayan classlar ıcın uygulanır.
     * Genellikle abstract class'lar temsil ettigi kavramın acık ve bagımsız bır kavram olmadıgında kullanılır.
     * Abstract class farklı eylemlerin uygulanması icin bir temel olarak calısır.
     * Abstract class'ların Interface'lerden farklı olarak, instance (object) veriable ve constructor icerebilmesidir.
     * Abstract class'lardan obje (nesne) olusturulamaz. Erismek icin baska bir sınıftan inherit (miras) edilmesi gerekir.
     * Abstract class'lar, guvenligi saglamak ıcın, belirli ayrıntıları gızleyıp objenın onemli ayrıntılarını gorsterır.
     * Abstract class'lar interface'leri ve inheritance'ı birlestirir.
     * Abstract class'lar body'leri olan normal (concrete) methodlar icerdigi gibi abstract method'lar da icerebilir.
     * Abstract method'ların sadece method signature (imzası-adı) vardır, body'si olmaz. Ici bostur. Body subclass tarafından saglanır.
     * Abstract methodu olan abstract class'lar duz(concrete) class'lar tarafından inherit edilirse subclass o abstract methodu implement etmek zorundadır.
     * Abstract class'ı extend eden class'lar ya abstract methodları ımplement etmeli ya da kendısını de abstract class yapmalıdır.
     *
     * 2- Interface - Arayuz
     *
     * Interface'leri, bir class'tan gerekli olan davranısı, yani method'larını tanımlamak icin kullanırız.
     * Interfac'ler davranısları method adları ve return degerlerı ıle tanımlarlar. Yani body'si olmayan method'lar ıcerirler.
     * Interfac'leri implement eden class'lar ınterface'deki tanımlanan methodları implement etmek zorundadırlar.
     * Interface'den ımplement edilen methodlar, nasıl uygulanacagına implement eden class'larda karar verilir.
     * Interface bir davranıs sozlesmesıdir. Bır class interface'ı uyguladıgında bır anlasma ımzalar.
     * Anlaşma, class'ın interface tarafından tanımlanan method'ları uygulayacağını (implement edecegini) belirtir.
     * Interface'ler gerekli method'ların yalnızca adlarını, parametrelerini ve return değerlerini tanımlar.
     * Interface'de olan methodlar her zaman gorunurluk erısebılırlık acısından public olmak zorundadır.
     *
     *
     */
}

