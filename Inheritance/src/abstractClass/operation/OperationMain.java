package abstractClass.operation;

import java.util.Scanner;

public class OperationMain {
    public static void main(String[] args) {


        Operation plusOperation = new PlusOperation();


        UserInterface ui = new UserInterface(new Scanner(System.in));
        ui.addOperation(plusOperation);
        ui.start();


    }
}
