package abstractClass.operation;

import java.util.Scanner;

public class PlusOperation extends Operation {


    public PlusOperation() {
        super("Plus Operation");
    }

    @Override
    public void execute(Scanner scanner) {
        System.out.print("First number: ");
        int first = scanner.nextInt();
        System.out.print("Second number: ");
        int second = scanner.nextInt();

        System.out.println("The sum of the numbers is " + (first + second));
    }

}
