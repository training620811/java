package abstractClass.operation;

import java.util.ArrayList;
import java.util.Scanner;

public class UserInterface {
    private Scanner scanner;
    private ArrayList<Operation> operations;

    public UserInterface(Scanner scanner) {
        this.scanner = scanner;
        this.operations = new ArrayList<>();
    }

    public void start() {

        while (true) {
            printOperation();
            System.out.print("Choice: ");
            int choice = scanner.nextInt();
            if (choice == 0) {
                break;
            }
            executeOperation(choice);
            System.out.println();
        }
    }

    public void addOperation(Operation operation) {
        this.operations.add(operation);
    }

    public void printOperation() {
        System.out.println("Operations:");
        System.out.println("\t0: Stop");

        for (int i = 0; i < operations.size(); i++) {
            System.out.println("\t" +(i + 1) + ": " + operations.get(0).getName());
        }
    }

    public void executeOperation(int choice) {
        Operation chosen = this.operations.get(choice-1);
        chosen.execute(scanner);

    }
}
