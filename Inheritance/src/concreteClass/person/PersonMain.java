package concreteClass.person;

import java.util.ArrayList;


public class PersonMain {

    public static void main(String[] args) {

        ArrayList<Person> persons = new ArrayList<Person>();

        persons.add(new Student("Zeynep","Antalya"));
        persons.add(new Student("İsmail","Bursa"));
        persons.add(new Teacher("Ahmet","İstanbul",2500));

        Student student = new Student("Hakan","Tekirdağ");
        student.study();
        persons.add(student);


        Teacher teacher =new Teacher("Oya","Ankara",3000);
        persons.add(teacher);


        printPerson(persons);
    }
    public static void printPerson(ArrayList<Person> persons){
        for (Person person :persons){
            System.out.println(person);
        }
    }
}
