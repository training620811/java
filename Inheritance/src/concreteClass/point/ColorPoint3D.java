package concreteClass.point;

public class ColorPoint3D extends Point3D {
    private String color;

    public ColorPoint3D(int x, int y, int z, String color) {
        super(x, y, z);
        this.color= color;
    }

    @Override
    public String toString() {
        return super.toString() + " color : " + this.color;
    }
}
