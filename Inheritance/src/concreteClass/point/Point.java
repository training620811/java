package concreteClass.point;

public class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int distanceFromOrigin() {
        return Math.abs(x) + Math.abs(y);
    }

    protected String location() {
        return this.x + ", " + this.y;
    }

    @Override
    public String toString() {
        return "(" + this.location() + ") distance " + this.distanceFromOrigin();
    }
}
