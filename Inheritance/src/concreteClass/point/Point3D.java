package concreteClass.point;

public class Point3D extends Point {
    private int z;

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    @Override
    protected String location() {
        return super.location() + ", " + this.z;
    }

    @Override
    public int distanceFromOrigin() {
        return super.distanceFromOrigin() + Math.abs(z);
    }

}
