package interfaceExample.box;

public class Book implements Packable{

    private String author;
    private String name;
    private double weigth ;

    public Book(String author, String name, double weigth) {
        this.author = author;
        this.name = name;
        this.weigth = weigth;
    }

    @Override
    public double weight() {
        return this.weigth;
    }

    @Override
    public String toString() {
        return this.author+": "+this.name;
    }
}
