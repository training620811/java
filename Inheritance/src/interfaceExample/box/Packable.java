package interfaceExample.box;

public interface Packable {
    double weight();
}
