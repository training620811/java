package interfaceExample.readableThings;

public interface Readable {
    String read();

}
