package interfaceExample.readableThings;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ReadableMain {
    public static void main(String[] args) {


        ArrayList<Readable> readingList = new ArrayList<>();

        readingList.add(new TextMessage("ope","never been programming before..."));
        readingList.add(new TextMessage("ope", "gonna love it i think!"));
        readingList.add(new TextMessage("ope", "give me something more challenging! :)"));
        readingList.add(new TextMessage("ope", "you think i can do it?"));
        readingList.add(new TextMessage("ope", "up here we send several messages each day"));


        ArrayList<String> pages = new ArrayList<>();
        pages.add("A method can call itself.");

        Readable book=new  Ebook("Introduction to Recursion", pages);

        readingList.add (book);

        for ( Readable readable: readingList) {
            System.out.println(readable.read());
        }


//        ArrayList<String> pages = new ArrayList<>();
//        pages.add("Split your method into short, readable entities.");
//        pages.add("Separate the user-interface logic from the application logic.");
//        pages.add("Always program a small part initially that solves a part of the problem.");
//        pages.add("Practice makes the master. Try different out things for yourself and work on your own projects.");
//
//        Ebook book = new Ebook("Tips for programming.", pages);
//
//        int page = 0;
//        while (page < book.pages()) {
//            System.out.println(book.read());
//            page = page + 1;
//        }
    }
}