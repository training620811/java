package interfaceExample.tacoBox;

// Icınde ıstege baglı sayıda taco bulunduran ozel (custom) box.
public class CustomTacoBox implements TacoBox {
    private int taco;

    public CustomTacoBox(int taco) {
        this.taco = taco;
    }

    @Override
    public int tacosRemaining() {
        return this.taco;
    }

    @Override
    public void eat() {
        if(this.taco>0){
            this.taco--;
        }
    }

    @Override
    public void printCountTaco() {
        System.out.println("Taco count: " + this.tacosRemaining());
    }
}
