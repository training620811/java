package interfaceExample.tacoBox;

public interface TacoBox {
    int tacosRemaining();
    void eat();

    void printCountTaco();
}
