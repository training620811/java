package interfaceExample.tacoBox;

public class TacoBoxMain {
    public static void main(String[] args) {
        TacoBox tripleBox = new TripleTacoBox();
        TacoBox customBox = new CustomTacoBox(5);

        tripleBox.printCountTaco();
        tripleBox.eat();
        tripleBox.printCountTaco();
        System.out.println();
        customBox.printCountTaco();
        customBox.eat();
        customBox.eat();
        customBox.printCountTaco();


    }
}
