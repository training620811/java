package interfaceExample.tacoBox;

// Icınde 3 tane taco bulunduran (triple) box.
public class TripleTacoBox implements TacoBox {
    private int taco;

    public TripleTacoBox() {
        this.taco = 3;
    }

    @Override
    public int tacosRemaining() {
        return this.taco;
    }

    @Override
    public void eat() {
        if (taco > 0) {
            this.taco--;
        }
    }

    @Override
    public void printCountTaco() {
        System.out.println("Taco count: " + this.tacosRemaining() );
    }


}
