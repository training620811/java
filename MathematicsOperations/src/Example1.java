import java.util.Scanner;

public class Example1 {
    public static void main(String[] args) {
        // Kullanıcıdan alınan 3 basamaklı pozıtif bir tam sayının rakamlar toplamını bulun.

        System.out.println(23/4); // 4,6 ==> 4 dür. Cunku ınt ıle ınt boluyor, sonucu ınt yazıyor.
        System.out.println(123%10); // kalanı verir. 3
        System.out.println(123/10); // 12,3 ==> 12 dir. Sayılar ınt oldugu ıcın ondalık kısmını yazmaz.

        Scanner scan=new Scanner(System.in);
        System.out.println("Lutfen 3 basamaklı pozitif bir tamsayi giriniz :");
        int userNum = scan.nextInt();
        int birlerBasamagi= userNum%10;
        int onlarBasamagi=(userNum/10)%10;
        int yuzlerBasamagı=(userNum/100)%10;
        int toplam=birlerBasamagi+onlarBasamagi+yuzlerBasamagı;
        System.out.println("Girilen tamsayının rakamları toplamı :" + toplam);

    /////////////////////////////  YA DA //////////////////////////////////////////////

        System.out.println("Lutfen 3 basamaklı pozitif bir tamsayi giriniz :");
        int num =scan.nextInt();

        int birlerBas=0;
        int rakamToplami=0;

        // Birler Basamagi
        birlerBas=num%10;
        rakamToplami=rakamToplami+birlerBas;
        num=num/10; // Ondalik kismi almayacagi icin num'un yeni halini num veriable'ına deger verecek.

        // Onlar Basamagi
        birlerBas=num%10; // Yeni num'un kalanı yani birler basamagini verecek.
        rakamToplami=rakamToplami+birlerBas; // Son rakamtoplamina yeni birlerbasamagını ekleyecek.
        num=num/10;

        // Yuzler Basamagi
        birlerBas=num%10;
        rakamToplami=rakamToplami+birlerBas;
        num=num/10; // num'un 10'a bolumu 0 olacaktır. Yani ondalıklı kısım atılacak ve sadece 0 olacak.

        System.out.println("Girilen tamsayının rakamları toplamı :" + rakamToplami);


    }
}