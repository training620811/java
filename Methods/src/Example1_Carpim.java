import java.util.Scanner;

public class Example1_Carpim {
    public static void main(String[] args) {

        //1- Main method icerisinde kullanıcıdan iki sayi alin.
        // bu ıkı sayıyı parametre olarak kabul edip, carpimlarını main method 'a donduren
        // bır method olusturun.

        Scanner scan =new Scanner(System.in);
        System.out.println("Lutfen iki sayı giriniz :");
        double userNum1= scan.nextDouble();
        double userNum2= scan.nextDouble();
        System.out.println(carpimMethodu(userNum1,userNum2));

    }

    public static double carpimMethodu(double num1, double num2){

     return num1*num2;
    }
}
