import java.util.Scanner;

public class Example2_IsimSoyisimDuzenleme {
    public static void main(String[] args) {

        /* 2- Kullancidan main method icinde ayri ayri isim ve soyismini alin
              İsim ve soyismi ilk harfleri buyuk diger harfler kucuk olacak sekilde duzenleyip,
              isim bosluk soyisim seklinde bize donduren bır method olusturun.
              input : isim : Ali soyisim : YILMAZ   output: Ali Yilmaz
         */

        Scanner scan = new Scanner(System.in);

        System.out.print("Isminizi yaziniz : ");
        String userName = scan.nextLine();
        System.out.print("Soyisiminizi yaziniz : ");
        String userSurname =scan.nextLine();

        String result = nameSurnameRegulate(userName,userSurname);
        System.out.println(result);

    }

    public static String nameSurnameRegulate(String name, String surname){

        String newName = name.substring(0,1).toUpperCase()+
                         name.substring(1).toLowerCase();

        String newSurname = surname.substring(0,1).toUpperCase()+
                            surname.substring(1).toLowerCase();

     return newName + " " + newSurname;
    }
}
