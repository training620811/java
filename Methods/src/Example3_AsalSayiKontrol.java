import java.util.Scanner;

public class Example3_AsalSayiKontrol {
    public static void main(String[] args) {

        /* 3- Kullanicidan main method icinde pozitif bir tamsayi alin. Girilen sayinin
              asal sayi olup olmadıgını kontrol edip, sonuc olarak "asal sayi" veya
              "asal sayi degil" donuclarını donduren method olusturun.
         */

        Scanner scan =new Scanner(System.in);

        System.out.print("Lutfen pozitif bir tam sayi giriniz : ");
        int userNum = scan.nextInt();

        String result = primeNumberControl(userNum);
        System.out.println(result);

        System.out.println(primeNumberControl(5674532)); // asal degil
    }

    public static String primeNumberControl(int num){

        String message = "asal sayi";
        for (int i = 2; i <num ; i++) {
            if(num%i==0){
                message = "asal sayi degil";
                break;
            }
        }
        return message;
    }

}
