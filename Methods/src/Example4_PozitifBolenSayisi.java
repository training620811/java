import java.util.Scanner;

public class Example4_PozitifBolenSayisi {
    public static void main(String[] args) {

        /* 4- Kullanicidan main method icinde bır tam sayi alin. Girilen sayinin pozitif tam
              bolenleri sayisini bulup bize donduren bir method olusturun.
         */

        Scanner scan = new Scanner(System.in);
        System.out.print("Lutfen bir sayi giriniz : ");
        int userNum = scan.nextInt();

        System.out.println(userNum+ " sayisinin pozitif bolen sayisi : "+ numberOfPositiveDivisor(userNum));

    }

    public static int numberOfPositiveDivisor(int num) {

        int counter = 0;
        for (int i = 1; i < num + 1; i++) {
            if (num % i == 0) {
                counter++;
            }
        }
        return counter;
    }
}