public class Example5_StringTerseCevirme {
    public static void main(String[] args) {

        //5- Parametre olarak bır String kabul edip
        //   String'in terse cevrılmıs halını donduren bır method olusturun.

        String input = "Java kod yazdikca ogrenilir";
        String resultReversed= stringTerseCevirme(input);
        System.out.println(resultReversed);
    }

    public static String stringTerseCevirme(String text){

        String newText ="";

        for (int i = text.length()-1; i >=0 ; i--) {
            newText += text.charAt(i);

        }
        return newText;
    }
}
