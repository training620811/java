import java.util.Scanner;

public class Example7_KendiSubstringMetodunuOlusturma {
    public static void main(String[] args) {

        /* 7- Kullanıcıdan ınput olarak verilen bir String, baslangıc ve bitis indexlerine göre
              baslangıc index'i dahil, bitis index'i haric olacak sekilde aradaki harfleri
              yazdıran bir method olusturun.
              - kullanıcı baslangıc degeri olarak, bıtıs degerınden buyuk bır sayı gırerse,
                hata mesajı verin
              - kullanıcı str'da olan index'lerden daha buyuk bir index girerse hata mesaji
                yazdirin.
        */
        Scanner scan= new Scanner(System.in);
        System.out.print("Lutfen bir metin ifadesi yaziniz :");
        String userText = scan.nextLine();

        System.out.println("Yazdırmak istediginiz aralıgı belirtiniz. (Baslangıc 0'dan baslar) ");

        System.out.print("1. index :");
        int indexStartOfUser = scan.nextInt();
        System.out.print("2. index :");
        int indexfinishOfUser = scan.nextInt();

        myselfSubstringMethod(userText,indexStartOfUser,indexfinishOfUser);
        myselfSubstringMethod("Java",1,3);// av
        myselfSubstringMethod("Deneme",6,3); // Hata!
        myselfSubstringMethod("Java",6,8); // Hata!

    }

    public static void myselfSubstringMethod(String text, int indexStart, int indexFinish){
        if(indexStart>indexFinish){
            System.out.println("Baslangıc index'i, bitis index'inden buyuk olamaz !");
        }
        else if (indexFinish>text.length()-1) {
            System.out.println("Bitis indexi String'in sinirlari disinda !");
        }
        else{

            for (int i = indexStart; i <indexFinish ; i++) {
                System.out.print(text.charAt(i));
            }
            System.out.println("");
        }
    }
}