import java.util.Arrays;

public class Example10_HerInnerlarinElementlerininToplaminiYeniArrayeAtama {
    public static void main(String[] args) {

        //10- Verilen 2 katli bir array'de her bir ic array'deki elementleri toplayip,
        //    yeni olusturacagımız tek katli bir array'e bu toplamlari atayin.
        //    input: {{3,1,2,4},{1,2},{3,4,5},{10},{2,7}}   output: [10,3,12,10,9]

        int[][] arr = {{3, 1, 2, 4}, {1, 2}, {3, 4, 5}, {10}, {2, 7}};

        int[] toplamArr = new int[arr.length];


        for (int i = 0; i < arr.length; i++) { // inner arraylerı kontrol eder
            int innerArrayToplam = 0;
            for (int j = 0; j < arr[i].length; j++) { // her bır ınner arayın elementlerını
                innerArrayToplam += arr[i][j];                  // kontrol eder.
            }
            toplamArr[i] = innerArrayToplam;
        }
        System.out.println(Arrays.toString(toplamArr));
    }
}
