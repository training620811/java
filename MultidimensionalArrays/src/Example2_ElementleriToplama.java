public class Example2_ElementleriToplama {
    public static void main(String[] args) {


        int[]arr1 = {3,5,7,8}; // 23
        //1- elementleri topla

        int toplam1 =0;

        for (int i = 0; i <arr1.length ; i++) {
            toplam1+=arr1[i];
        }
        System.out.println("Tek katli array elementler toplami = " + toplam1);

        int[][] arr2 ={{1,2},{3,4,5},{7}}; // 22
        //1- elementleri topla

        int toplam2=0;

        for (int i = 0; i <arr2.length ; i++) {
            for (int j = 0; j <arr2[i].length ; j++) {
                toplam2+=arr2[i][j];
            }
        }
        System.out.println("Cok katli array elementler toplami = " + toplam2);
    }
}
