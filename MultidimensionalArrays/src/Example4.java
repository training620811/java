import java.util.Arrays;
public class Example4 {
    public static void main(String[] args) {

        //4- Turkiyedeki bolgeleri(3) ve o bolgelerdeki illeri(3) iceren ornegi yazdırın.

        String[][] sehirler = new String[3][3];

        sehirler[0][0] = "İstanbul";
        sehirler[0][1] = "Bursa";
        sehirler[0][2] = "Tekirdag";
        sehirler[1][0] = "İzmir";
        sehirler[1][1] = "Muğla";
        sehirler[1][2] = "Denizli";
        sehirler[2][0] = "Antalya";
        sehirler[2][1] = "Hatay";
        sehirler[2][2] = "Mersin";

        System.out.println(Arrays.deepToString(sehirler));

        System.out.println("Marmara bolgesindeki sahirler :");
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < sehirler[0].length; j++) {
                System.out.println(sehirler[i][j]);
            }
        }
        //    System.out.println(sehirler[0][0]);
        //    System.out.println(sehirler[0][1]);
        //    System.out.println(sehirler[0][2]);


        System.out.println("Ege bolgesindeki sahirler :");
        for (int i = 1; i < 2; i++) {
            for (int j = 0; j < sehirler[1].length; j++) {
                System.out.println(sehirler[i][j]);
            }
        }
        System.out.println("Akdeniz bolgesindeki sahirler :");
        for (int i = 2; i < 3; i++) {
            for (int j = 0; j < sehirler[2].length; j++) {
                System.out.println(sehirler[i][j]);
            }
        }

    }
}
