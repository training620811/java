public class Example6_CiftSayiAdediVeToplami {
    public static void main(String[] args) {

        //6- Verilen iki katlı bir ınteger array'deki cift sayı adedını ve toplamini
        //   bulan kod yazınız.
        //   {{4,6},{3,5,8},{1,0,4}}


        int[][] arr= {{4,6},{3,5,8},{1,0,4}};

        int counter = 0;
        int toplam =0;

        for (int i = 0; i <arr.length ; i++) {
            for (int j = 0; j <arr[i].length ; j++) {
                if (arr[i][j]%2==0){
                    counter++;
                    toplam+=arr[i][j];
                }
            }
        }
        System.out.println("Cift sayi adedi : " + counter); // 5
        System.out.println("Ciftsayilar toplami : "+ toplam); // 22
    }
}
