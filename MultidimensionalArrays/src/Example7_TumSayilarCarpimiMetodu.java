public class Example7_TumSayilarCarpimiMetodu {
    public static void main(String[] args) {

        //7- Verilen 2 katli bir array’de bulunan
        //   tum sayilarin carpimini bize donduren bir method olusturun.
        //   {{5,7}, {5, 8, 9},{0,1}}

        int[][] arr = {{5, 7}, {5, 8, 9}, {0, 1}};

        int result = sayilarCarpimi(arr);
        System.out.println("Sayilar carpimi = " + result);

    }

    public static int sayilarCarpimi(int[][] array) {

        int carpim = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                carpim *= array[i][j];
            }
        }
        return carpim;
    }
}

