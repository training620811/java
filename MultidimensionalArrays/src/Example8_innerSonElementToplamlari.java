public class Example8_innerSonElementToplamlari {
    public static void main(String[] args) {

        //8- Verilen 2 katli bir array’de
        //   her bir inner array’in son elementlerinin toplaminini yazdiran bir method olusturun.
        //   {{5,7}, {5, 8, 9},{0,1}}

        int[][] arr = {{5, 7}, {5, 8, 9}, {0, 1}};
        sonElementlerToplami(arr);

    }

    public static void sonElementlerToplami(int[][] array) {

        int toplam = 0;

        for (int i = 0; i < array.length; i++) {
            toplam += array[i][array[i].length - 1];

        }
        System.out.println("Inner array'lerin son elementler carpimi : " + toplam);
    }
}
