import java.util.Arrays;

public class Example9_AyniIndexElementlerToplaminiYeniArrayeAtama {
    public static void main(String[] args) {

        //9- Verilen 2 katli bir array'de ayni index'e sahip elementleri toplayip,
        //   yeni olusturacagimiz tek katli bir array e bu toplamlari atayin.
        //      input :  {{3,4,5},{2,3,6,7}}    output : [5,7,11]

        int[][] arr = {{3, 4, 5}, {2, 3, 6, 7}};

        int minOrtakIndexSayisi = arr[0].length;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length < minOrtakIndexSayisi) {
                minOrtakIndexSayisi = arr[i].length;
            }
        }
        System.out.println("Ortak index sayisi : " + minOrtakIndexSayisi);

        int[] ortakIndexToplamArr = new int[minOrtakIndexSayisi];

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < minOrtakIndexSayisi; j++) {

                ortakIndexToplamArr[j] = arr[i][j] + arr[i + 1][j];

            }


        }
        System.out.println(Arrays.toString(ortakIndexToplamArr));

    }
}
