import java.util.Arrays;
import java.util.Scanner;

public class BreakContınue {
    public static void main(String[] args) {

        // 1 - Asagidaki programlari yaziniz 50 puan):
        //
        //  Kullanicidan bir string aliyoruz.
        //  O stringeki karakterlerden sadece sesli harfleri yazdiriyoruz
        //  Stringdeki karakerlerdedn sadece sessiz harfleri yazdiriyoruz
        //  Stringdeki karakterlerden sadece ilk sessiz harfi yazdirip sonra loopdan cikiyoruz
        //
        //  (Continue ve break kullanmanizi bekliyoruz)

        char[] sesliHarfler = {'a', 'e', 'o', 'ö', 'u', 'ü', 'i', 'ı'};
        char[] sessizHarfler = {'b', 'c', 'ç', 'd', 'f', 'g', 'ğ', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 'ş', 't', 'v', 'y', 'z', 'x', 'w', 'q'};

        String deneme ="codingbook";

        Scanner scan =new Scanner(System.in);
        System.out.println("Lutfen bir metin giriniz : ");
        String userStr = scan.nextLine();

        String sesliHarfListesi = "";
        String sessizHarfListesi ="";
        String ilkSessizHarf = "";

        for (int i = 0; i <userStr.length() ; i++) {

            for (int j = 0; j <sesliHarfler.length; j++) {

                if(userStr.toLowerCase().charAt(i) != sesliHarfler[j]){
                    continue;
                }
                sesliHarfListesi += userStr.charAt(i);
            }

            for (int j = 0; j <sessizHarfler.length ; j++) {

                if (userStr.toLowerCase().charAt(i) != sessizHarfler[j]){
                    continue;

                }
                sessizHarfListesi += userStr.charAt(i);
            }
        }

        boolean cikis =true;

        for (int i = 0; i <userStr.length() && cikis ; i++) {

            for (int j = 0; j <sessizHarfler.length ; j++) {
                if (userStr.toLowerCase().charAt(i) == sessizHarfler[j]){
                    ilkSessizHarf+= userStr.charAt(i);
                    cikis = false;
                    break;
                }
            }
        }

        System.out.println("Sesli harfler listesi : "+ Arrays.toString(sesliHarfListesi.toCharArray()) );
        System.out.println("Sessiz harfler listesi : "+ Arrays.toString(sessizHarfListesi.toCharArray()) );
        System.out.println("Ilk sessiz harf : "+ Arrays.toString(ilkSessizHarf.toCharArray()) );
    }
}
