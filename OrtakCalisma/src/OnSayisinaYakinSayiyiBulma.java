import java.util.Scanner;

public class OnSayisinaYakinSayiyiBulma {

     /*  Kullanicidan 1-20 arasi  2 int alin. Verilen 2 degerden Hangisinin 10’a daha yakin oldugunu tespit edin.
            Mesela kullanici (3,8) verdi, bu durumda 8 daha yakindir, (14,9) verdi ise 9 daha yakindir.
            Esitlik saglandigi durumda,  ornk= (8,12)  uyari versin..
            Kullanıcı eger 1-20 aralıgı dısında deger girerse uyarı verip basa donup tekrar kullanıcıdan 1. ve 2. sayıyı talep etsin
            Tum bunları yapan bir method yazın.
         */

    public static void main(String[] args) {

        //     onaEnYakinSayiyiBulma();

        String mesaj = onaEnYakinSayiyiBulmaReturn();
        System.out.println(mesaj);

    }

    public static void onaEnYakinSayiyiBulma() {

        Scanner scan = new Scanner(System.in);

        for (int i = 0; i >= 0; i++) {

            System.out.println("10 haric 1-20 arasi 1. sayinizi giriniz :");
            int userNum1 = scan.nextInt();
            System.out.println("10 haric 1-20 arasi 2. sayinizi giriniz :");
            int userNum2 = scan.nextInt();

            int userNum1Farki = Math.abs(10 - userNum1);
            int userNum2Farki = Math.abs(10 - userNum2);

            if (userNum1 < 21 && userNum2 < 21 && userNum1 > 0 && userNum2 > 0 && userNum2 != 10 && userNum1 != 10) {

                if (userNum1Farki > userNum2Farki) {
                    System.out.println(userNum2 + " sayisi 10'a daha yakındır.");
                } else if (userNum1Farki < userNum2Farki) {
                    System.out.println(userNum1 + " sayısı 10'a daha yakindir.");
                } else {
                    System.out.println(userNum1 + " ve " + userNum2 + " sayisinın 10 sayisina uzaklıgı esittir.");
                }
                break;
            } else {
                System.out.println("Gecersiz sayi girdiniz.");
                System.out.println("");
            }
        }
    }

    public static String onaEnYakinSayiyiBulmaReturn() {
        Scanner scan = new Scanner(System.in);

        String sonuc = "";

        for (int i = 0; i >= 0; i++) {

            System.out.println("10 haric 1-20 arasi 1. sayinizi giriniz :");
            int userNum1 = scan.nextInt();
            System.out.println("10 haric 1-20 arasi 2. sayinizi giriniz :");
            int userNum2 = scan.nextInt();

            int userNum1Farki = Math.abs(10 - userNum1);
            int userNum2Farki = Math.abs(10 - userNum2);

            if (userNum1 < 21 && userNum2 < 21 && userNum1 > 0 && userNum2 > 0 && userNum2 != 10 && userNum1 != 10) {

                if (userNum1Farki > userNum2Farki) {
                    sonuc = userNum2 + " sayisi 10'a daha yakındır.";

                } else if (userNum1Farki < userNum2Farki) {
                    sonuc = userNum1 + " sayısı 10'a daha yakindir.";

                } else {
                    sonuc = userNum1 + " ve " + userNum2 + " sayisinın 10 sayisina uzaklıgı esittir.";
                }
                break;
            }
            else {
                System.out.println("Gecersiz sayi girdiniz.");
                System.out.println("");
            }
        }
        return sonuc;
    }
}
