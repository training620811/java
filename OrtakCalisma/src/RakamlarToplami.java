import java.util.Scanner;

public class RakamlarToplami {

    /*
         Kullanicidan bir sayi aliniz.
         Alınan sayının rakamlar toplamını bulan bir method olusturunuz.
         Olusturulan methodu main'de cagırıp return edip sonucu ekrana yazdırınız.

     */

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Lutfen bir sayi giriniz : ");
        int userNum = scan.nextInt();
        int rakamlarToplami = rakamlarToplaminiBulma(userNum);
        System.out.println("Girilen sayinin rakamlar toplami : " + rakamlarToplami);

    }

    public static int rakamlarToplaminiBulma(int num) {
        int birlerBasamagi = 0;
        int rakamlarToplami = 0;

        while (num > 0) {

            birlerBasamagi = num % 10;
            rakamlarToplami += birlerBasamagi;
            num = num / 10;
        }
        return rakamlarToplami;
    }
}
