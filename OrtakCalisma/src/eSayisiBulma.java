import java.util.Scanner;

public class eSayisiBulma {
    public static void main(String[] args) {

        // Kullanıcıdan alınan bir string ifadede kac tane e harfi oldugunu bulan bir method yazınız.

        Scanner scan= new Scanner(System.in);

        System.out.println("Lutfen bir metin giriniz :");
        String userText = scan.nextLine();

        int result = eSayisi(userText);
        System.out.println("Girilen metindeki 'e' sayisi : " + result);

    }


    public static int eSayisi(String text){                     // e sayısını bulan metot

        String [] textArr = text.split("");

        int eCount =0;
        for (int i = 0; i < textArr.length; i++) {
            if(textArr[i].equalsIgnoreCase("e")){
                eCount++;
            }
        }
        return eCount;
    }
}
