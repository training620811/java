import java.util.Scanner;

public class Example1 {
    public static void main(String[] args) {

        /*1- mail kontrolu yapan bır program hazirlayin.
             1- mail. @ isareti icermiyorsa "gecersiz email"
             2- @gmail.com icermiyorsa "gmail adresi giriniz"
             3- @gmail.com ile bitmiyorsa "yazim hatasi"
             seklınde sonuc yazdırın.
         */

        Scanner scan = new Scanner(System.in);

        System.out.println("E-mail adresinizi giriniz :");
        String userEmail = scan.nextLine();


        if (!userEmail.contains("@")) {
            System.out.println("Gecersiz e-mail");
        } else if (!userEmail.contains("@gmail.com")) {
            System.out.println("gmail adresi giriniz");
        } else if (!userEmail.endsWith("@gmail.com")) {
            System.out.println("Yazim hatasi");
        } else {
            System.out.println("email basari ile kaydedildi.");
        }
    }
}
