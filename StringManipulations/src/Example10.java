import java.util.Locale;
import java.util.Scanner;

public class Example10 {
    public static void main(String[] args) {

        /* 10- Kullanıcıdan isim ve soyısmi ayrı ayrı alın
                -ısmı daha uzun ıse, ısım ve soyismi ilk harf buyuk kalanlar kucuk sekılde yazdırın
                -esit veya soyısım daha uzun ise
                 ismi ilk harf buyuk dıgerelerı kucuk, soyısmı buyuk harflerle yazdırın.
         */

        Scanner scan = new Scanner(System.in);

        System.out.println("Lutfen isminizi giriniz :");
        String name = scan.nextLine();
        System.out.println("Lutfen soyısmınızı giriniz :");
        String surName = scan.nextLine();

        String newName, newSurname;

        if (name.length() > surName.length()) {
            newName = name.substring(0, 1).toUpperCase() +
                    name.substring(1).toLowerCase();

            newSurname = surName.substring(0, 1).toUpperCase() +
                    surName.substring(1).toLowerCase();

            System.out.println(newName + " " + newSurname);

        } else {

            newName = name.substring(0, 1).toUpperCase() +
                    name.substring(1).toLowerCase();

            newSurname = surName.toUpperCase();

            System.out.println(newName + " " + newSurname);

        }
    }
}
