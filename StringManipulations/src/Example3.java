import java.util.Scanner;

public class Example3 {
    public static void main(String[] args) {

        //3- Kullanicidan bir cumle ve bir metin alin cumlede metnin durumuna gore
        //      1- cumle metni icermiyor
        //      2- cumle metni sadece 1 kere iceriyor
        //      3- cumle metni birden fazla iceriyor seceneklerinden uygun olani yazdirin

        Scanner scan = new Scanner(System.in);

        System.out.println("Bir cumle yazınız :");
        String userCumle=scan.nextLine();

        System.out.println("Bir metin yazınız :");
        String userMetin = scan.nextLine();

        if(!userCumle.contains(userMetin)){
            System.out.println("cumle metni icermiyor");
        } else {
            int ilkIndex= userCumle.indexOf(userMetin);
            int sonrakiIndex = userCumle.indexOf(userMetin,ilkIndex+1);

            if (sonrakiIndex==-1){
                System.out.println("Cumle metni bir defa iceriyor");
            }else {
                System.out.println("Cumle metni birden fazla ıcerıyor");
            }
        }
    }
}
