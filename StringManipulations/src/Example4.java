import java.util.Scanner;

public class Example4 {
    public static void main(String[] args) {

        //4- Kullanicidan bir cumle ve bir metin alin cumlede metnin durumuna gore
        //      1- cumle metni icermiyor
        //      2- cumle metni sadece 1 kere iceriyor
        //      3- cumle metni birden fazla iceriyor seceneklerinden uygun olani yazdirin
        //      (indexOf ve lastındexOf kullanarak cozum)

        Scanner scan = new Scanner(System.in);

        System.out.println("Bir cumle yazınız :");
        String userCumle = scan.nextLine();

        System.out.println("Bir metin yazınız :");
        String userMetin = scan.nextLine();

        int ilkIndex = userCumle.indexOf(userMetin);
        int sonIndex = userCumle.lastIndexOf(userMetin);

        if (ilkIndex == -1) {
            System.out.println("cumle metni icermiyor");
        } else if (ilkIndex == sonIndex) {
            System.out.println("Cumle metni bir defa iceriyor");
        } else {
            System.out.println("Cumle metni birden fazla iceriyor");
        }
    }
}
