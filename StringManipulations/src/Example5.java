public class Example5 {
    public static void main(String[] args) {

        //  Kullanıcının girdigi metinde
        //  harf dısında kalan tum karakterlerı temızleyen bır kod yazın
        //  Not  : space sılınmemeli

        String input = "Ja5+va  co_k 1*guzel"; // deneme ıcındır. Kullanıcıdan alınabılır.

        input = input.replaceAll("\\d",""); // Ja+va cok *guzel
        input = input.replace(" ", "1"); // Ja+va1cok1*guzel
        input = input.replaceAll("\\W",""); // Java1cok1guzel
        input= input.replace("_",""); // _ kaldırdı
        input = input.replace("1"," ");
        input = input.replaceAll("\\s"," "); // bırden fazla space ı tek space yaptı

        System.out.println(input);
    }
}
