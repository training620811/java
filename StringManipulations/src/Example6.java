import java.util.Scanner;

public class Example6 {
    public static void main(String[] args) {

        /*6- Kullanıcıdan bır cumle alın
             - cumlede ev geciyorsa, "home home sweet home"
             - cumlede is gecıyorsa, "calismak guzeldir"
             - ikisini de iceriyorsa, "hem ev lazim hem is"
             - hicbirini icermiyorsa, "cok calısman lazım" yazdırın.
         */

        Scanner scan = new Scanner(System.in);

        System.out.println("Bir cumle yaziniz :");
        String userText = scan.nextLine();

        String userTextCopy = userText.toLowerCase(); // IgnoreCase ıcın


        if(userTextCopy.contains("ev") && userTextCopy.contains("is")){
            System.out.println("hem ev lazim hem is");
        }else if (userTextCopy.contains("is")){
            System.out.println("calismak guzeldir");
        }else if (userTextCopy.contains("ev")) {
            System.out.println("home home sweet home");
        }else {
            System.out.println("cok calısman lazım");
        }
    }
}
