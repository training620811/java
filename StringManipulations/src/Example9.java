import java.util.Scanner;

public class Example9 {
    public static void main(String[] args) {

        /* 9- Kullanıcıdan bır sıfre ısteyıp, asagıdaki sartları kontrol edın ve kullanıcıya
              duzeltmesi gereken tum eksıklerı soyleyın, eger tum sartları saglarsa,
              "sifre basariyla kaydedildi" yazdirin.
              -ilk harf kucuk harf olmali
              -son karakter rakam olmali
              -sifre bosluk icermemeli
              -uzunlugu en az 10 karakter olmali
        */

        Scanner scan =new Scanner(System.in);

        System.out.println("Lutfen sıfrenızı gırınız :");

        String password = scan.nextLine();
        boolean checkTrue =true;

        if(!(password.charAt(0) >= 'a' && password.charAt(0)<='z')){
            System.out.println("İlk karakter kucuk harf olmalı");
            checkTrue=false;
        }
        if(!(password.charAt(password.length()-1)>='0' && password.charAt(password.length()-1)<='9')){
            System.out.println("Son karakter rakam olmalı");
            checkTrue=false;
        }
        if(password.contains(" ")){
            System.out.println("Sıfre bosluk ıcermemeli");
            checkTrue=false;
        }
        if(password.length()<10){
            System.out.println("Sıfre en az 10 karakter olmalı");
            checkTrue=false;
        }

        if (checkTrue==true){
            System.out.println("Sıfrenız basarı ıle kaydedıldı.");
        }
    }
}
