public class Main {
    public static void main(String[] args) {

        String str = "Java ogren, isi kap";

        String sonHarf = str.substring(str.length()-1);
        System.out.println(sonHarf); // p

        // son ındexdeki karakteri upper case yazdırın.

        System.out.println(str.substring(str.length()-1).toUpperCase()); // P

        // son 3 harfi buyuk harf olarak yazdırın.

        System.out.println(str.substring(str.length()-3).toUpperCase()); // KAP
    }
}