import java.util.Arrays;

public class StringExplanationsAndMethods {
    public static void main(String[] args) {

        String mesaj = "Bugün hava çok güzel";
        System.out.println(mesaj);

        // str.length() : Girilen karakterin sayisini dondurur.
        System.out.println("Eleman sayisi : " + mesaj.length());

        // str.charAt(5) : indexOf metoduna benzer sekilde
        //                 bu sefer konumu (index) verilenkarakteri okur.
        System.out.println("5. eleman sayisi : " + mesaj.charAt(4));

        // str1.concat(str2) : String birleştirme. Sadece String veriable'ları birlestirir.
        System.out.println(mesaj.concat(" degil mi ?"));
        System.out.println(mesaj); // yine "Bugün hava çok güzel" yazar.

        // str.starsWith("a") : Belirtilen karakter(ler) ile basliyorsa
        //                      true degerını dondurur. Yazdırmak ya degıskene atamak gerekir.
        //                      'toffset' kısmı da istenilen yerden sonrasının baslangıcını
        //                       sorgulamak icin kullanılır.
        System.out.println(mesaj.startsWith("B")); // true
        System.out.println(mesaj.startsWith("g",2)); // true

        // str.endsWith("a") : Belirtilen karakter(ler) ile bıtıyorsa
        //                     true degerını dondurur. Yazdırmak ya degıskene atamak gerekir.
        System.out.println(mesaj.endsWith("l")); // true

        // str.getChars(srcBegin: , srcEnd: , char [] dst => degisken adı, dstBegin: )
        // karakterleri almak ıcındır. Void'tır bır sey dondurmez.
        char[] karakter = new char[5];
        mesaj.getChars(0, 5, karakter, 0);
        System.out.println(karakter); // "Bugün"


        // str.indexOf('K') : Karakterin konumunu (index) verir.
        //                    eğer karakter yoksa -1 döner.

        System.out.println(mesaj.indexOf('a')); // 7 ch da string de kabul eder "a" gibi
        System.out.println(mesaj.indexOf("hava")); // 6 blok olarak alır ılkharfın ındexını verır.
        System.out.println(mesaj.indexOf("a",7)); // 7 den sonra "a" arar.


        // str.lastIndexOf("av") : aramaya sondan baslayıp ındex ı verır.

        System.out.println(mesaj.lastIndexOf("e")); // 18
        System.out.println(mesaj.lastIndexOf("a")); // 9 sondan bakıp ılk a yı soyledi

        // str.replace() : string degıstırme için kullanılır.
        //                 sadece char degıl string de degistirir.
        //                 target : " ", replacement : "" bosluklaro kaldırır.
        //                 birden fazla replace arka arkaya yazılabılır
        //                 replace.replace.replace....
        //                 eski stringi degıstırmez. Atama yapmak gerekır ya da sout
        System.out.println(mesaj.replace(' ', '-')); // yeni metin verir.
        System.out.println(mesaj.replace("a","")); // a ları siler.
        // str.replaceAll() : sadece bır harf veya char sequence'i degıl genelleme ile
        //                    soyleyebilecegımız ortak ozellıkteki tum karakterleri degıstırır.
        //                    char kullanılmaz strıng kullanılır.
        // regex(regular Expressions) : duzenlı kısaltmalar.
        //  \\s : space , \\S : space olmayan hersey
        //  \\s+: yanyana bırden fazla space
        //  \\d : digits (sayilar) , \\D : digit olmayan hersey
        //  \\w : harf veya rakam ve altcızgı , \\W : harf veya rakam ve altcızgı olmayan hersey
        String str = "J1ava2 G9uzel5dir8";
        System.out.println(str.replaceAll("\\d" , "")); // Java Guzeldir
        System.out.println(str.replaceAll("\\D", "")); // 12958

        String s1 = "ilk urun fiyatı : 1250 tl";
        String s2 = "ikinci urun fiyatı : 1500 tl";
        s1 =s1.replaceAll("\\D",""); // 1250
        s2 = s2.replaceAll("\\D",""); // 1500

        System.out.println(Integer.parseInt(s1)+Integer.parseInt(s2)); //2750 (warper clası)



        // str.substring() : Strıngde belirlenen yerden sonrasını istenilen yere kadar
        //                   almak ıcın. İlk ındexi alır sonuncusunu almaz
        System.out.println(mesaj.substring(2)); // "gün hava çok güzel"
        System.out.println(mesaj.substring(2, 4)); // "gü"

        // str.split () : bir String belirli bir karakter ya da karakter dızısıni
        //                dikkate alarak parcalamaya yarar ve dızı (array) olarak verır.
        System.out.println(Arrays.toString(mesaj.split(" "))); // 1.yol
        for (String kelime : mesaj.split(" ")) {               // 2.yol
            System.out.println(kelime);
        }


        // str.toLowerCase () : String'in karakterlerini kucuk harf yapar
        // str.toUpperCase () : String'in karakterlerini buyuk harf yapar
        System.out.println(mesaj.toLowerCase());
        System.out.println(mesaj.toUpperCase());

        // str.trim () : Strıngın basındakı ya da sonundakı boslukları siler.
        String mesaj2 = "   Bugün hava cok güzel   ";
        System.out.println(mesaj2.trim());


        // str.compareTo("Java") : Buyuk/kucuk harf duyarlı olarak karsılastırır.
        // str.compareToIgnoreCase ("JavA") : Buyuk/Kucuk harf duyarlı olmadan karsılastırır.

        // str.contains("cok") : girilen deger strıng ıcınde gecıyor mu dıye kontrol eder.
        //                       boolean doner. Ya veriableye deger olarak atamak ya da print etmek
        //                       gerekir.
        System.out.println(mesaj.contains("çok")); // ture

        // str.equals(str2) : String'lerde metınlerın aynı alıp olmadıgını karsılastırmak ıcın
        //                    == yerine equals() kullanılır.

        // str.equalsIgnoreCase(str1) : Strıng karsılastırılırken buyuk/kucuk harf duyarlı
        //                              olmadan kontrol eder. Kelimede bosluk vs tolere etmez.

        // str.isEmpty() : Verilen stringin bos olup olmadıgını boolean olarak dondurur.
        // str.isBlank() : Stringin bosluklardan olusup olusmadıgını boolean olarak dondurur.

        // null pointer  : Bir deger degıl isaretcıdır. String isim=null; olarak gosterilir.
        //                 stack memory de referans olusturulur fakat heap memoryde
        //                 obje olusturmaz. Sadece referans ve pointer olusturur.
        //                 methodla kullanılmaz, izin vermez.


        // str.repeat() :  Metni istediginiz kadar tekrarlar
        System.out.println(str.repeat(3));

    }
}
