import java.util.Scanner;
public class Example2 {
    public static void main(String[] args) {

        // 2- Kullanicidan gun numarasini alip, hafta ici veya hafta sonu yazdirin.

        Scanner scan= new Scanner(System.in);

        System.out.println("Lutfen gun numarasini giriniz :");
        int dayNum =scan.nextInt();

        switch (dayNum){        // switch de break koymadigimiz surece diger case lere de bakar
            case 1 :            // ve eger bir komut varsa onu da yapar.
            case 2 :
            case 3 :
            case 4 :
            case 5:
                System.out.println("Hafta ici");
                break;
            case 6:
            case 7 :
                System.out.println("Hafta sonu");
                break;
            default:
                System.out.println("Gecersiz gun numarasi");
        }
    }
}
