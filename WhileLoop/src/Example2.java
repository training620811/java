import java.util.Scanner;

public class Example2 {
    public static void main(String[] args) {

        /*
        Kullanıcıdan toplanmak uzere tam sayılar alın
        kullanıcı 0'a basarsa sayı alma ısemını bıtrın

        kullanıcının kac sayı gırdıgını
        ve bu sayıların toplamının kac oldugunu yazdırın
        */

        Scanner scan = new Scanner(System.in);

        boolean exit = true;
        int sayac = 0;
        int toplam = 0;

        while (exit) {

            System.out.println("Lutfen toplamak ıcın tamsayı gırın" +
                    "\nBıtırmek ıcın 0'a basin");
            int userNum = scan.nextInt();
            if (userNum==0){
                exit=false;
            }else{
                sayac++;
                toplam+=userNum;
            }
        }
        System.out.println("Girilen " + sayac + " sayinin toplamı : "+ toplam);
    }
}