import java.util.Scanner;

public class Example3 {
    public static void main(String[] args) {

        /*3- Kullanıcıdan toplanmak uzere sayi alin sayıların toplamı 500'e esıt
             olur veya gecerse grilen sayi adedı, girilen sayıların toplamını ve
             "bu kadar yeter" yazdırın.
        */

        Scanner scan = new Scanner(System.in);

        int sayac =0;
        int toplam =0;

        while  (toplam<=500){
            System.out.println("Lutfen sayinizi giriniz:");
            int sayi= scan.nextInt();
            toplam+=sayi;
            sayac++;
        }
        System.out.println("Girilen "+ sayac+ " sayinin toplami : " + toplam +" oldu. Bu kadar yeter.");

    }

}
