import java.util.Scanner;

public class Example4 {
    public static void main(String[] args) {

        /* 4-Kullanicidan sifre isteyin asagidaki sartlari saglamayan sifrelerde hatalari yazdirip,
             kullanicinin yeni sifre girmesini isteyin. Gecerli bir sifre yazilincaya kadar bu islemi
             tekrar edin. Gecerli sifre yazilinca "sifreniz basari ile kaydedildi" yazdirin.

             sartlar :
             - sifrenin ilk karakteri kucuk harf olmali
             - sifrenin son karakteri sayi olmali
             - sifre en az 8 karakter olmali
         */

        Scanner scan = new Scanner(System.in);

        boolean sifreDogruMu = true;
        String sifre = "";
        char ilkKarakter;
        char sonKarakter;

        while (sifreDogruMu==true) {
            int bayrak = 0;
            System.out.println("Lutfen sifrenizi giriniz.");
            sifre = scan.nextLine();
            ilkKarakter = sifre.charAt(0);
            sonKarakter = sifre.charAt(sifre.length() - 1);

            if (ilkKarakter < 'a' || ilkKarakter > 'z') {
                System.out.println("Sifrenizin ilk karakteri kucuk harf olmali");
                bayrak++;

            }
            if (sonKarakter < '0' || sonKarakter > '9') {
                System.out.println("Sifrenizin son karakteri sayi olmali");
                bayrak++;
            }
            if(sifre.length()<8){
                System.out.println("Sifre en az 8 karakter olmali");
                bayrak++;
            }
            if (bayrak == 0) {
                System.out.println("Sifreniz basari ile kaydedildi.");
                sifreDogruMu = false;
            }
        }
    }
}
