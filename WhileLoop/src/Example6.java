public class Example6 {
    public static void main(String[] args) {

        //7- While loop kullanarak kullanıcıdan alınan sayının rakamlar toplamını bulun.

        int sayi = 1453;
        int birlerBasamagi = 0;
        int rakamlarToplami = 0;

        while (sayi!=0) {

            birlerBasamagi = sayi % 10;
            rakamlarToplami+=birlerBasamagi;
            sayi =sayi/10;
        }
        System.out.println("Girilen sayinin rakamlar toplami : " + rakamlarToplami);
    }
}